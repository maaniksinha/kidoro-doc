// if (!window.DOMTokenList) {
//   Element.prototype.containsClass = function(name) {
//     return new RegExp("(?:^|\\s+)" + name + "(?:\\s+|$)").test(this.className);
//   };

//   Element.prototype.addClass = function(name) {
//     if (!this.containsClass(name)) {
//       var c = this.className;
//       this.className = c ? [c, name].join(' ') : name;
//     }
//   };

//   Element.prototype.removeClass = function(name) {
//     if (this.containsClass(name)) {
//       var c = this.className;
//       this.className = c.replace(
//           new RegExp("(?:^|\\s+)" + name + "(?:\\s+|$)", "g"), "");
//     }
//   };
// }

// sse.php sends messages with text/event-stream mimetype.
var source = new EventSource('http://www.kidorohealth.com/SSE/sse.php');

// function Logger(id) {
//   this.el = document.getElementById(id);
// }

// Logger.prototype.log = function(msg, opt_class) {
//   var fragment = document.createDocumentFragment();
//   var p = document.createElement('p');
//   p.className = opt_class || 'info';
//   p.textContent = msg;
//   fragment.appendChild(p);
//   this.el.appendChild(fragment);
// };

// Logger.prototype.clear = function() {
//   this.el.textContent = '';
// };

// var logger = new Logger('log');

function closeConnection() {
  source.close();
  console.log('> Connection was closed');
  // updateConnectionStatus('Disconnected', false);
}

// function updateConnectionStatus(msg, connected) {
//   var el = document.querySelector('#connection');
//   if (connected) {
//     if (el.classList) {
//       el.classList.add('connected');
//       el.classList.remove('disconnected');
//     } else {
//       el.addClass('connected');
//       el.removeClass('disconnected');
//     }
//   } else {
//     if (el.classList) {
//       el.classList.remove('connected');
//       el.classList.add('disconnected');
//     } else {
//       el.removeClass('connected');
//       el.addClass('disconnected');
//     }
//   }
//   el.innerHTML = msg + '<div></div>';
// }

source.addEventListener('message', function(event) {
  var data = JSON.parse(event.data);

  // var d = new Date(data.msg * 1e3);
  // var timeStr = [d.getHours(), d.getMinutes(), d.getSeconds()].join(':');

  // coolclock.render(d.getHours(), d.getMinutes(), d.getSeconds());

  console.log('lastEventID: ' + event.lastEventId +
             ', server time: ' + data, 'msg');
  console.log(data);
}, false);

source.addEventListener('open', function(event) {
   console.log('> Connection was opened');
  // updateConnectionStatus('Connected', true);
}, false);

source.addEventListener('error', function(event) {
  if (event.eventPhase == 2) { //EventSource.CLOSED
     console.log('> Connection was closed');
    // updateConnectionStatus('Disconnected', false);
  }
}, false);

// var coolclock = CoolClock.findAndCreateClocks();