(function(){
  "use strict";
  angular
    .module('flexcalendar', [])
    .directive('flexCalendar', flexCalendar);

    function flexCalendar() {

      var template =
      '<div id ="mydiv"><div id= "flexcalht" class="flex-calendar">'+
     '<div class="row dateCol"><div class="col col-30"  ng-click="changeView(divClosed)"><i class="icon ion-ios-calendar-outline" style="font-size: 20px;"></i></div><div class="col col-40">{{selectedYear}}</i></div><div class="col col-30"  ng-click="goToToday(day,$index, $event)"><i class="icon ion-social-tumblr">oday</i></div></div>'+
        '<div class="month">'+
          '<div class="arrow {{arrowPrevClass}}" ng-click="prevMonth()"></div>'+
          '<div class="label">{{selectedMonth}}</div>'+
          '<div class="arrow {{arrowNextClass}}" ng-click="nextMonth()"></div>'+
        '</div>'+
        '<div class="week">'+
          '<div class="day" ng-repeat="day in weekDays(options.dayNamesLength) track by $index">{{ day }}</div>'+
        '</div>'+
        '<div class="days" ng-repeat="week in weeks">'+
          '<div class="day"'+
            'ng-repeat="day in week track by $index"'+
            'ng-class="{selected: isDefaultDate(day), event: day.event[0], disabled: day.disabled, out: !day}"'+
            'ng-click="onClick(day, $index, $event)"'+
          '>'+
            '<div class="number">{{day.day}}</div>'+
          '</div>'+
        '</div>'+
      '</div>'+
      '</div>';

      var directive = {
        restrict: 'E',
        scope: {
          options: '=?',
          events: '=?'
        },
        template: template,
        controller: Controller
      };

      return directive;

    }

    Controller.$inject = ['$scope' , '$filter'];

    function Controller($scope , $filter) {

          angular.element(mydiv).css('height', 110 + "px");
          angular.element(mydiv).css('background-color', "#F5F7F6");
          angular.element(mydiv).css('color',"#111");
          angular.element(mydiv).css('font-weight',"bolder");
          angular.element(mydiv).css('font-size',"15px");
          // angular.element(hideBackground).css('background-color',"#F5F7F6");
          // angular.element(hideBackground).css('height',"300px");
              $scope.wView = "Week View";
              $scope.mView = "Month View";



    $scope.changeView = function(divClosed) {
      $scope.dynHeight = document.getElementById('flexcalht').offsetHeight;
      if ($scope.dynHeight>195) {
      $scope.dynHeightChanged = $scope.dynHeight;
      }
      if ($scope.dynHeightChanged === undefined) {
        $scope.dynHeightChanged = 280;
      }

                if($scope.weekView){
                  // console.log($scope.dynHeightChanged);

          $scope.weekView=false;
                calculateWeeks();
                $scope.divClosed = false;
                        $scope.wView = "Week View";
                        $scope.mView = "Month View";
                        // angular.element(mydiv).css('height', 140 + "px");
                        // angular.element(mydiv).css('background-color', "#ffb835");
                        // angular.element(hideBackground).css('background-color',"#F5F7F6");
                        // angular.element(hideBackground).css('height',"300px");
                        angular.element(mydiv).css('height', $scope.dynHeightChanged + "px");
                        angular.element(mydiv).css('background-color', "#F5F7F6");
                        // angular.element(hideBackground).css('background-color',"#F5F7F6");
                        // angular.element(hideBackground).css('height',dynHeightChanged+"px");
        }
        else if($scope.weekView===undefined){


          $scope.weekView=false;
          calculateWeeks();
          // $scope.weekView=true;
                    $scope.divClosed = true;
                        $scope.wView = "Month View";
                        $scope.mView = "Week View";
                        //  angular.element(mydiv).css('height', 140 + "px");
                        // angular.element(mydiv).css('background-color', "#ffb835");
                        // angular.element(hideBackground).css('background-color',"#F5F7F6");
                        // angular.element(hideBackground).css('height',"300px");
                        angular.element(mydiv).css('height', 280 + "px");
                        angular.element(mydiv).css('background-color', "#F5F7F6");
                        angular.element(hideBackground).css('background-color',"#F5F7F6");
                        // angular.element(hideBackground).css('height',"300px");
        }
        else{

          $scope.weekView=true;
          calculateWeeks();
                    $scope.divClosed = true;
                        $scope.wView = "Month View";
                        $scope.mView = "Week View";
                         angular.element(mydiv).css('height', 110 + "px");
                        angular.element(mydiv).css('background-color', "#F5F7F6");
                        angular.element(hideBackground).css('background-color',"#F5F7F6");
                        // angular.element(hideBackground).css('height',"10px");
                        // angular.element(mydiv).css('height', 380 + "px");
                        // angular.element(mydiv).css('background-color', "#009999");
                        // angular.element(hideBackground).css('background-color',"#F5F7F6");
                        // angular.element(hideBackground).css('height',"10px");

        }


    }

      $scope.days = [];
      $scope.options = $scope.options || {};
      $scope.events = $scope.events || [];
      $scope.options.dayNamesLength = $scope.options.dayNamesLength || 1;
      $scope.options.mondayIsFirstDay = $scope.options.mondayIsFirstDay || false;

      $scope.onClick = onClick;
      $scope.allowedPrevMonth = allowedPrevMonth;
      $scope.allowedNextMonth = allowedNextMonth;
      $scope.weekDays = weekDays;
      $scope.isDefaultDate = isDefaultDate;
      $scope.prevMonth = prevMonth;
      $scope.nextMonth = nextMonth;


      $scope.arrowPrevClass = "visible";
      $scope.arrowNextClass = "visible";
      $scope.weekView = $scope.options.weekView;
      $scope.goToToday = function(date, index, domEvent) {
        var date={"date":new Date()};
        $scope.options.defaultDate =new Date();
        $scope.options.dateClick(date, domEvent);
        // console.log(date);
        
      }
      // var $translate = $filter('translate');

      var MONTHS = ['JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'];
      var WEEKDAYS = ['SUNDAY' , 'MONDAY' , 'TUESDAY' , 'WEDNESDAY' , 'THURSDAY' , 'FRIDAY' , 'SATURDAY'];

      if($scope.options.mondayIsFirstDay)
      {
        //shift weeks..
        // var date = new Date().getDay();
        // for (var i = 0; i<date; i++) {
         var sunday = WEEKDAYS.shift();
          WEEKDAYS.push(sunday)
        // };


      }

      if ($scope.options.minDate) {
        $scope.options.minDate = new Date($scope.options.minDate);
      }

      if ($scope.options.maxDate) {
        $scope.options.maxDate = new Date($scope.options.maxDate);
      }

      if($scope.options.disabledDates) {
        createMappedDisabledDates();
      }

      if($scope.events)
      {
        createMappedEvents();
      }

      function createMappedDisabledDates(){
        $scope.mappedDisabledDates = $scope.options.disabledDates.map(function(date)
        {
          return new Date(date);
        });
      }

      function createMappedEvents(){
        $scope.mappedEvents = $scope.events.map(function(obj)
        {
          obj.date = new Date(obj.date);
          return obj;
        });
      }

      $scope.$watch('options.defaultDate', function() {
        calculateSelectedDate();
      });

      $scope.$watch('options.disabledDates', function() {
        if($scope.options.disabledDates) {
            createMappedDisabledDates();
            calculateDisabledDates();
        }
      });

      $scope.$watch('events', function() {
        createMappedEvents();
        calculateWeeks();
      });

      /////////////////

      function onClick(date, index, domEvent) {
        if (!date || date.disabled) { return; }
        $scope.options.defaultDate = date.date;
        if (date.event.length != 0) {
          $scope.options.eventClick(date, domEvent);
        }
        else
        {
          $scope.options.dateClick(date, domEvent);
        }
      }

      function bindEvent(date) {
        if (!date || !$scope.mappedEvents) { return; }
        date.event = [];
        $scope.mappedEvents.forEach(function(event) {
          if (date.date.getFullYear() === event.date.getFullYear()
              && date.date.getMonth() === event.date.getMonth()
              && date.date.getDate() === event.date.getDate()) {
            date.event.push(event);
          }
        });
      }

      function allowedDate(date) {
        if (!$scope.options.minDate && !$scope.options.maxDate) {
          return true;
        }
        var currDate = date.date;
        if ($scope.options.minDate && (currDate < $scope.options.minDate)) { return false; }
        if ($scope.options.maxDate && (currDate > $scope.options.maxDate)) { return false; }
        return true;
      }

      function disabledDate(date) {
        if (!$scope.mappedDisabledDates) return false;
        for(var i = 0; i < $scope.mappedDisabledDates.length; i++){
          if(date.year === $scope.mappedDisabledDates[i].getFullYear() && date.month === $scope.mappedDisabledDates[i].getMonth() && date.day === $scope.mappedDisabledDates[i].getDate()){
            return true;
            break;
          }
        }
      }

      function allowedPrevMonth() {
        var prevYear = null;
        var prevMonth = null;
        if (!$scope.options.minDate) { return true; }
        var currMonth = MONTHS.indexOf($scope.selectedMonth);
        if (currMonth === 0) {
          prevYear = ($scope.selectedYear - 1);
          prevMonth = 11;
        } else {
          prevYear = $scope.selectedYear;
          prevMonth = (currMonth - 1);
        }
        if (prevYear < $scope.options.minDate.getFullYear()) { return false; }
        if (prevYear === $scope.options.minDate.getFullYear()) {
          if (prevMonth < $scope.options.minDate.getMonth()) { return false; }
        }
        return true;
      }

      function allowedNextMonth() {
        var nextYear = null;
        var nextMonth = null;
        if (!$scope.options.maxDate) { return true; }
        var currMonth = MONTHS.indexOf($scope.selectedMonth);
        if (currMonth === 11) {
          nextYear = ($scope.selectedYear + 1);
          nextMonth = 0;
        } else {
          nextYear = $scope.selectedYear;
          nextMonth = (currMonth + 1);
        }
        if (nextYear > $scope.options.maxDate.getFullYear()) { return false; }
        if (nextYear === $scope.options.maxDate.getFullYear()) {
          if (nextMonth > $scope.options.maxDate.getMonth()) { return false; }
        }
        return true;
      }

      function calculateWeeks() {
        var onCalWeeksdynHeight = document.getElementById('flexcalht').offsetHeight;

        // console.log("calculateWeeks"+$scope.weekView );

        $scope.weeks = [];
        var week = null;
        var startDate;
        var endDate;
        var daysInCurrentMonth = new Date($scope.selectedYear, MONTHS.indexOf($scope.selectedMonth) + 1, 0).getDate();
        if($scope.weekView){
          //   startDate = 1;
          // endDate = daysInCurrentMonth + 1;
          var tempDate = new Date();
          tempDate.setMonth(MONTHS.indexOf($scope.selectedMonth));
          tempDate.setYear($scope.selectedYear);
          // console.log(tempDate);
          var dayNumber = tempDate.getDay();
          startDate = new Date().getDate()-dayNumber;
          if(startDate<=0){
            startDate=1;
          }
          endDate = startDate+7+dayNumber;
        }
        else if($scope.weekView==undefined){
          // startDate = 1;
          // endDate = daysInCurrentMonth + 1;
           var tempDate = new Date();
          tempDate.setMonth(MONTHS.indexOf($scope.selectedMonth));
          tempDate.setYear($scope.selectedYear);
          // console.log(tempDate);
          var dayNumber = tempDate.getDay();
          startDate = new Date().getDate()-dayNumber;
           if(startDate<=0){
            startDate=1;
          }
          endDate = startDate+7+dayNumber;
        }
        else{
          startDate = 1;
          endDate = daysInCurrentMonth + 1;
          // var tempDate = new Date();
          // tempDate.setMonth(MONTHS.indexOf($scope.selectedMonth));
          // tempDate.setYear($scope.selectedYear);
          // console.log(tempDate);
          // var dayNumber = tempDate.getDay();
          // startDate = new Date().getDate()-dayNumber;
          // endDate = startDate+7+dayNumber;
        }
        for (var day = startDate; day < endDate; day += 1) {
          var date = new Date($scope.selectedYear, MONTHS.indexOf($scope.selectedMonth), day);
          var dayNumber = new Date($scope.selectedYear, MONTHS.indexOf($scope.selectedMonth), day).getDay();
          if($scope.options.mondayIsFirstDay)
          {
            dayNumber = (dayNumber + 6) % 7;
          }
          week = week || [null, null, null, null, null, null, null];
          week[dayNumber] = {
            year: $scope.selectedYear,
            month: MONTHS.indexOf($scope.selectedMonth),
            day: day,
            date: date,
            _month : date.getMonth() + 1
          };

          if (allowedDate(week[dayNumber])) {
            if ($scope.mappedEvents) { bindEvent(week[dayNumber]); }
          } else {
            week[dayNumber].disabled = true;
          }

          if (week[dayNumber] && disabledDate(week[dayNumber])) {
            week[dayNumber].disabled = true;
          }

          if (dayNumber === 6 || day === daysInCurrentMonth) {
            $scope.weeks.push(week);
            week = undefined;
          }
        }
        (!$scope.allowedPrevMonth()) ? $scope.arrowPrevClass = "hidden" : $scope.arrowPrevClass = "visible";
        (!$scope.allowedNextMonth()) ? $scope.arrowNextClass = "hidden" : $scope.arrowNextClass = "visible";
      }

      function calculateSelectedDate() {
        if ($scope.options.defaultDate) {
          $scope.options._defaultDate = new Date($scope.options.defaultDate);
        } else {
          $scope.options._defaultDate = new Date();
        }

        $scope.selectedYear  = $scope.options._defaultDate.getFullYear();
        $scope.selectedMonth = MONTHS[$scope.options._defaultDate.getMonth()];
        $scope.selectedDay   = $scope.options._defaultDate.getDate();
        // calculateWeeks();
      }

      function calculateDisabledDates() {
        if (!$scope.mappedDisabledDates || $scope.mappedDisabledDates.length === 0) return;
        for(var i = 0; i < $scope.mappedDisabledDates.length; i++){
          $scope.mappedDisabledDates[i] = new Date($scope.mappedDisabledDates[i]);
        }
        calculateWeeks();
      }

      function weekDays(size) {
        // return WEEKDAYS.map(function(name) { return $translate(name).slice(0, size); });

        return WEEKDAYS.map(function(name) { return name.slice(0, size); });
      }

      function isDefaultDate(date) {
        if (!date) { return; }
        var result = date.year === $scope.options._defaultDate.getFullYear() &&
          date.month === $scope.options._defaultDate.getMonth() &&
          date.day === $scope.options._defaultDate.getDate();
        return result;
      }

      function prevMonth() {
      var onprevdynHeight = document.getElementById('flexcalht').offsetHeight;

        if (!$scope.allowedPrevMonth()) { return; }
        var currIndex = MONTHS.indexOf($scope.selectedMonth);
        if (currIndex === 0) {
          $scope.selectedYear -= 1;
          $scope.selectedMonth = MONTHS[11];
        } else {
          $scope.selectedMonth = MONTHS[currIndex - 1];
        }
        var month = {name: $scope.selectedMonth, index: currIndex - 1, _index: currIndex+2 };
        $scope.options.changeMonth(month, $scope.selectedYear);
        calculateWeeks();
      }

      function nextMonth() {
        var onnextdynHeight = document.getElementById('flexcalht').offsetHeight;

        if (!$scope.allowedNextMonth()) { return; }
        var currIndex = MONTHS.indexOf($scope.selectedMonth);
        if (currIndex === 11) {
          $scope.selectedYear += 1;
          $scope.selectedMonth = MONTHS[0];
        } else {
              $scope.selectedMonth = MONTHS[currIndex + 1];
        }
        var month = {name: $scope.selectedMonth, index: currIndex + 1, _index: currIndex+2 };
        $scope.options.changeMonth(month, $scope.selectedYear);
        calculateWeeks();
      }



    }



})();
