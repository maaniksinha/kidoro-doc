angular.module('starter.controllers', ['kinvey'])

.controller('chatCtrl', function ($scope, $ionicScrollDelegate,$cordovaLocalNotification, $cordovaToast,$state, $ionicLoading, privateMessage, userDataService, passPatientDetails) {


  // private chat with doctors
    try{
      $scope.sendMessageClicked=0;

   $scope.scrollBottom = function() {
             cordova.plugins.Keyboard.show();
             var isVisible = cordova.plugins.Keyboard.isVisible()

             if (isVisible == true) {

               $ionicScrollDelegate.scrollBottom(true);

             }
           }

            $scope.user = userDataService.getData();
            $scope.patientData = passPatientDetails.getData();

            $scope.senderID = $scope.user.Doctor_ID + "&" + $scope.patientData.Kidoro_Code;
            $scope.cssUser = $scope.user.Doctor_ID;
            $scope.messages= privateMessage.all($scope.senderID);

            $ionicLoading.show({});
            $scope.messages.$loaded().then(function(messages) {
              $ionicScrollDelegate.scrollBottom(true);
              $ionicLoading.hide();
            });

               $scope.newmessage = {
                 text: "",
                 user: "",
                 kidoroCode:"",
                 time: "",
                 displayTime: "",
                 dp: ""

               }

               $scope.sendMessage = function(message){
                  $scope.sendMessageClicke=1;
                 var time = new Date().getTime();
                 var date = new Date(time);

                 var hours = date.getHours();
                 var minutes = "0" + date.getMinutes();
                 var formattedTime = hours + ':' + minutes.substr(-2);

                 if ($scope.user.Doc_Pic === undefined || $scope.user.Doc_Pic == "" || $scope.user.Doc_Pic == null) {

                   $scope.newmessage = {
                     text: message.text,
                     user: $scope.user.Doc_Name,
                     kidoroCode: $scope.cssUser,
                     time: time,
                     displayTime: formattedTime,
                     dp: null
                   }
                   privateMessage.create($scope.newmessage, $scope.senderID);

                      } else {

                     $scope.newmessage = {
                     text: message.text,
                     user: $scope.user.Doc_Name,
                     kidoroCode: $scope.cssUser,
                     time: time,
                     displayTime: formattedTime,
                     dp: $scope.user.Doc_Pic
                     }
                     privateMessage.create($scope.newmessage, $scope.senderID);
                   }
                 $scope.messages= privateMessage.all($scope.senderID);
                 $scope.messages.$loaded().then(function(messages) {
                 });
                   delete $scope.newmessage.text;
                   cordova.plugins.Keyboard.disableScroll(true)
                   var isVisible = cordova.plugins.Keyboard.isVisible()

                   if (isVisible == true) {
                     cordova.plugins.Keyboard.close()
                     $ionicScrollDelegate.scrollBottom(true);

                   }
                   cordova.plugins.Keyboard.close()
               };



    }catch(err){
        $ionicLoading.hide();
        $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
        }, function (error) {
        });
    }




})

.controller('tempSplashCtrl', function ($scope, $state) {

})

.controller('loginHomeCtrl', function($scope,$ionicViewService, $ionicSlideBoxDelegate, $cordovaToast,$kinvey, $state, $ionicHistory) {
try {

      $ionicViewService.clearHistory();
      $scope.images = [
                      {imgUrl: "./img/slide1.png", txt1:"Appointments", txt2:"Manage your clients appointments"},
                      {imgUrl: "./img/slide2.png", txt1:"Appointments", txt2:"Manage your clients appointments"},
                      {imgUrl: "./img/slide3.png", txt1:"Appointments", txt2:"Manage your clients appointments"}
                    ];
    $ionicSlideBoxDelegate.update();

    $scope.goToLogin = function() {
            $state.go('welcomeLogin');
    }

    $scope.goToRegister = function() {
      $state.go('registerHome');
    }


}catch(err){
    $ionicLoading.hide();
    $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
    }, function (error) {
    });
}
})


.controller('welcomeLoginCtrl', function($scope, $state,$ionicLoading,$kinvey,getPatientDetails,userDataService,$ionicPopup){
  try {
    $scope.user={
                  username:null,
                  kidoroCode:null
                };


  $scope.forgotPassword = function() {
        $state.go('forgotPassword');

  }

  $scope.goToRegister = function() {
    $state.go('registerHome');
  }

 $scope.goToHomePage = function() {
   var user = $kinvey.getActiveUser();
      $ionicLoading.show({});

            var promise = $kinvey.User.login({
                              username: Number($scope.user.username),
                              password: $scope.user.kidoroCode
                          });
                          promise.then(
                              function (response) {
                                  $scope.submittedError = false;
                                  if(response.AccountType=="Doctor"){

                                  var query = new $kinvey.Query();
                                  query.equalTo('Kidoro_Doc_Code', response.kidoroCode);
                                  var promise = $kinvey.DataStore.find('KidoroDocs', query);
                                  promise.then(function(models) {

                                  $ionicLoading.hide();
                                  getPatientDetails.getData(models[0].Doctor_ID);
                                  userDataService.setData(models[0]);

                                  if(models[0].Account_Status=="activated"){
                                     $state.go('menu.appointments');
                                  }
                                  else{
                                    var errorAlert = $ionicPopup.alert({
                                    title: 'Kidoro Health',
                                    template: "Your Account is not active",
                                    cssClass:'custom-popup-alert',
                                     buttons: [{
                                     text: "Ok",
                                     type: 'button-alert-ok',
                                     onTap: function(e) {
                                       errorAlert.close();


                                     }
                                   }]
                                 });
                                  }

                                  }, function(err) {
                                  $ionicLoading.hide();
                                      $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
         }, function (error) {
        });

                          });

                      }
             else{ }


                    },
                    function (error) {
                    $ionicLoading.hide();
                    $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
         }, function (error) {
        });
      });
    }
  }catch(err){
      $ionicLoading.hide();
      $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
        }, function (error) {
        });
    }
  })


.controller('registerHomeCtrl', function($scope, $state, userDataService,$cordovaToast){

  try{

          $scope.user={
                  fullName:null,
                  mobileNumber:null,
                  email:null,
                  practiceName:null,
                  address:null,
                  locality:null
                };

    userDataService.setData($scope.user);

    $scope.nextRegister = function() {

      $state.go('registerHomeNext');
    }


     }catch(err){
          $ionicLoading.hide();
          $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
              
          }, function (error) {
             
          });
      }

})


.controller('registerHomeNextCtrl', function($scope, $state,$cordovaToast, userDataService){

try{

  $scope.user={
              fullName:null,
              mobileNumber:null,
              email:null,
              practiceName:null,
              address:null,
              locality:null
            };

    $scope.user = userDataService.getData();

    $scope.registerSuccess = function() {
    userDataService.setData($scope.user);
      $state.go('registerSuccess');
    }


    }catch(err){
          $ionicLoading.hide();
          $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
           }, function (error) {
           });
      }
      
 })




.controller('registerSuccessCtrl', function($scope, $state, $timeout,userDataService,$cordovaToast, $ionicLoading,$kinvey) {

try{

  function formatNumber(n){
      return n > 9 ? "" + n: "0" + n;
  }

   function zeroPadding(n) {
                    if(n<10)
                    return ("000" + n);
                    else if (n>9 && n<100)
                    return ("00" + n);
                    else if (n>99 && n<1000)
                    return("0" + n);
                    else return(n);
                }

  var getKidoroCode = function(){

  var hashids = new Hashids(($scope.user.mobileNumber).toString(), 5, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
  var currentDate = new Date();

  var date=Number(currentDate.getDay().toString()+currentDate.getMonth().toString()+(Number(currentDate.getFullYear()%100).toString()));

  var id = hashids.encode(date);
  var code="DOC"+id;
  return code;
  }
  $scope.user = userDataService.getData();
   $ionicLoading.show({});
    $scope.kidoroCode = getKidoroCode();

           var promise = $kinvey.User.signup({
                   username: Number($scope.user.mobileNumber),
                   password: $scope.kidoroCode,
                   email:$scope.user.email,
                   fullName:$scope.user.fullName,
                   MobileNumber:Number($scope.user.mobileNumber),
                   kidoroCode:$scope.kidoroCode,
                   AccountType:"Doctor"
               });

              promise.then(
              function (model) {
                //Kinvey signup finished with success
                $scope.submittedError = false;
                  var totalCount;
                 var promise = $kinvey.DataStore.count('KidoroDocs');
                 promise.then(function(response) {
                  $scope.totalCount = ++response;
                  $scope.count = "DOC"+zeroPadding($scope.totalCount);


                 var promise = $kinvey.DataStore.save('KidoroDocs', {
                  Doctor_ID:  $scope.count,
                  Doc_Name : $scope.user.fullName,
                  Doc_Phone : Number($scope.user.mobileNumber),
                  Doc_Email : $scope.user.email,
                  Kidoro_Doc_Code :$scope.kidoroCode,
                  Doc_Locality:$scope.user.locality,
                  Doc_Address:$scope.user.address,
                  Speciality:$scope.user.practiceName,
                  Specialization:$scope.user.practiceName,
                  Account_Status:"Not Active"

                  });
                  promise.then(function(model) {

                    $scope.user = model;
                  // send welcome mail on sign up
                    var html = "<p><b>Dear" + " " + $scope.user.Doc_Name + ",</b></p></br>" + "<p>Thank you for sign up with <b>Kidoro Health.</b></p>";
                    var text = "Congratulations! Welcome to Kidoro Health";
                    var subject = "Welcome to Kidoro Health";
                    var email = $scope.user.Doc_Email;
                    var template_name = "welcome-email-kidoro";
                    var name = $scope.user.Doc_Name;
                  
                  // send welcome mail on sign up
                  var promise = $kinvey.User.logout();
                  promise.then(function() {
                    $ionicLoading.hide();
                  }, function(error) {
                              //Kinvey signup finished with error
                              $ionicLoading.hide();
                              $scope.submittedError = true;
                              // alert("Oops !" +error.description);
                              $scope.errorDescription = error.description;

                  });

                  }, function(err) {
                    $ionicLoading.hide();

             $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
        }, function (error) {
        });


                  });


                }, function(err) {
                    $ionicLoading.hide();

             $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
        }, function (error) {
        });

                  });

              },
              function(error) {
                              //Kinvey signup finished with error
                              $ionicLoading.hide();
                              $scope.submittedError = true;
                              // alert("Oops !" +error.description);
                              $scope.errorDescription = error.description;

              });
      }catch(err){
      $ionicLoading.hide();
      $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
        
      }, function (error) {
          
      });
  }

})



.controller('forgotPasswordCtrl', function($scope, $state, $cordovaToast,$ionicLoading, $kinvey){



try{
   $scope.user={"mobileNumber":null};

     $scope.goToLogin = function() {
         $ionicLoading.show({});
   // check for existing user

   var promise = $kinvey.User.exists(Number($scope.user.mobileNumber));
   promise.then(function(response) {
     if(response==true){

          var query = new $kinvey.Query();
         query.equalTo('Doc_Phone',Number($scope.user.mobileNumber));
          var promise = $kinvey.DataStore.find('KidoroDocs', query);

        promise.then(function(models) {
          var kidoroCode=models[0].Kidoro_Code;
          var authKey="91802AFF83DNnxK55fc18a1";

            $http.get("https://control.msg91.com/api/sendhttp.php?authkey="+authKey+"&mobiles="+models[0].User_Phone+"&message="+"Your Kidoro Code is "+kidoroCode+"&sender=Kidoro&route=4&country=91")
           .success(function(result) {
            $ionicLoading.hide();

             $state.go('menu.welcomeLogin');
         })
           .error(function(data) {
            $ionicLoading.hide();

               $state.go('menu.welcomeLogin');
               return data;
         });
        },
         function(err) {
         $ionicLoading.hide();
        });
     }
     else {
       $ionicLoading.hide();
     }

   }, function(err) {
      $ionicLoading.hide();

     $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
         }, function (error) {
        });
      });

   }


}catch(err){
      $ionicLoading.hide();
      $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
      }, function (error) {
      });
  }

 
})






.controller('MenuCtrl', function($scope, $ionicModal, $timeout, passPatientDetails,$cordovaToast, $kinvey, $ionicLoading, passDoctorDetails,userDataService) {

try{

 $scope.user=userDataService.getData();
  $scope.doctorId =$scope.user.Doctor_ID;
  var query = new Kinvey.Query();
    query.equalTo('Doctor_ID', $scope.doctorId);
    var promise = $kinvey.DataStore.find('KidoroDocs',query);
    promise.then(function(model) {
      $scope.doctorDetails = model[0];
      if ($scope.doctorDetails.Specialization != null) {
          $scope.doctorDetails.Specialization=$scope.doctorDetails.Specialization.replace("^", ', ');
      }

      if ($scope.doctorDetails.Qualification != null) {
          $scope.doctorDetails["qual"]=$scope.doctorDetails.Qualification.replace("^", ', ');
      }

      passDoctorDetails.setData($scope.doctorDetails);

    },function(err){
             // alert('Failed '+err.description);
      });

}catch(err){
      $ionicLoading.hide();
      $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
      }, function (error) {
      });
  }
 
})


.controller('AppointmentCtrl', function($scope,$state, $ionicLoading,$cordovaToast,  $timeout, $interval,$location,$kinvey, $ionicPopup, $ionicModal, passPatientDetails,kinveyInitService,userDataService,$ionicHistory) {

try{
   $ionicHistory.clearHistory();
      $scope.previousRecordsLength=null;
      $scope.updateCount=null;
      $scope.user = userDataService.getData();
      var currentDate = new Date();
      $scope.selectedDate = currentDate;
      var todayDateString = currentDate.getFullYear()+"-"+pad(currentDate.getMonth()+1)+"-"+currentDate.getDate();
       $scope.divClosed = true;
     
          $scope.options = {
            defaultDate: todayDateString,
            minDate: "2015-01-01",
            maxDate: "2020-12-31",
           
            dayNamesLength: 3, 
            mondayIsFirstDay: false,
            eventClick: function(date) {
            },
            dateClick: function(date) {
              var dateSelected= date.date;
              $scope.getAppointments(dateSelected);
              $scope.selectedDate = dateSelected;
            },
            changeMonth: function(month, year) {
            }
          };

          $scope.events = [
            {foo: 'bar', date: "2015-08-18"},
            {foo: 'bar', date: "2015-12-01"}
          ];


function pad(d) {
    return (d < 10) ? '0' + d.toString() : d.toString();
}

var getTimeFromString = function(timeString){
  var tString = timeString.trim().split(" ");
  var min;
  if(tString[1]=="AM"){
     var timeValString = tString[0].split(":");

       if(Number(timeValString[0])==12){
       min = ((Number(timeValString[0])-12)*60)+(Number(timeValString[1]));
        }
        else{
           min = ((Number(timeValString[0]))*60)+(Number(timeValString[1]));
        }

    }
    else{
     var timeValString = tString[0].split(":");

      if(Number(timeValString[0])==12){
       min = ((Number(timeValString[0]))*60)+(Number(timeValString[1]));
        }
        else{
           min = ((Number(timeValString[0])+12)*60)+(Number(timeValString[1]));
        }

  }
  return min;

}
//init function
  $scope.doctorId = $scope.user.Doctor_ID;
  var todayDate = new Date();
  var arrD = new Array("SUN","MON","TUE","WED","THUR","FRI","SAT");

  $scope.getConsultationFee = function(){

   for (var i = 0; i < $scope.appointmentList.length; i++) {
    var Location_ID = $scope.appointmentList[i]["Location_ID"];
    var query = new $kinvey.Query();
    query.equalTo('Location_ID', Location_ID);
    var promise = $kinvey.DataStore.find('KidoroLocation',query);
    promise.then(function(model){
      for (var i = 0; i < $scope.appointmentList.length; i++){

        if($scope.appointmentList[i]["Location_ID"]==model[0].Location_ID){
      $scope.appointmentList[i]["Consultaion_Fee"] = model[0].Consultation_Fee;
      }
     }
    },
    function(err) {
    });


  };
}
$scope.selectedHospital={
  "hospitalName":null,
      "Location_ID":null
}
$scope.createHospitalNameList=function(){
  // $ionicLoading.show();
  $scope.hospitalNameList=[];
    var query = new $kinvey.Query();
    query.equalTo('Doctor_ID', $scope.doctorId);

    var promise = $kinvey.DataStore.find('KidoroLocation',query);
    promise.then(function(model){


for(i=0;i<model.length;i++){
    var temp={
      "hospitalName":model[i].Hospital_Name,
      "Location_ID":model[i].Location_ID
    }
    $scope.hospitalNameList.push(temp);
  }

$scope.selectedHospital=$scope.hospitalNameList[0];
    },function(err){
    });

}
$scope.hospitalChanged=function(object){
$scope.selectedHospital=object;
}
$scope.groupHospitals = function(){
$scope.hospitalGroup=[];
for (var i = 0; i < $scope.appointmentList.length; i++) {
  var timeString = $scope.appointmentList[i].Appointment_Time;
  var hours= Number(timeString.split(":")[0]);
  if(hours>12){
    hours=hours-12;
  }
  var AppointmentTime=hours+":"+timeString.split(":")[1];

$scope.appointmentList[i]["formattedTime"]=AppointmentTime;
        if($scope.hospitalGroup.length==0){
        var tempAppointmentItem=$scope.appointmentList[i];
        var tempItem = {
                        'hospitalName':$scope.appointmentList[i]["Hospital_Name"],
                        'appointmentItems' :[],
                        'length': 1,
                        'Location_ID':$scope.appointmentList[i]["Location_ID"]
                        };
        tempItem["appointmentItems"].push(tempAppointmentItem);
        $scope.hospitalGroup.push(tempItem);

      }
      else{
        for (var j = $scope.hospitalGroup.length - 1; j >= 0; j--) {
          if($scope.hospitalGroup[j]["hospitalName"]==$scope.appointmentList[i]["Hospital_Name"]){
            $scope.hospitalGroup[j]["appointmentItems"].push($scope.appointmentList[i]);
            $scope.hospitalGroup[j]["length"]++;
            $scope.hospitalGroup[j]["Location_ID"]=$scope.appointmentList[i]["Location_ID"];
            break;
          }
          else{
            var tempAppointmentItem=$scope.appointmentList[i];
            var tempItem = {
                        'hospitalName':$scope.appointmentList[i]["Hospital_Name"],
                        'appointmentItems' :[],
                        'length': 1,
                        'Location_ID':$scope.appointmentList[i]["Location_ID"]
                        };
            tempItem["appointmentItems"].push(tempAppointmentItem);
            $scope.hospitalGroup.push(tempItem);
            break;
          }

        };

      }
      };

}

$scope.populateUserDetails = function(){
  for (var j = $scope.appointmentList.length - 1; j >= 0; j--) {

    $scope.userId = $scope.appointmentList[j].User_ID;
    var query = new $kinvey.Query();
    query.equalTo('Kidoro_Code', $scope.userId);

    var promise = $kinvey.DataStore.find('KidoroUser',query);
    promise.then(function(model){

      if(model.length>0){
      $scope.user=model;
      for (var i = $scope.appointmentList.length - 1; i >= 0; i--){
        if($scope.appointmentList[i].User_ID==$scope.user[0].Kidoro_Code){
        $scope.appointmentList[i]["Alllegies"]=$scope.user[0].Alllegies;
        $scope.appointmentList[i]["Child_Date"]=$scope.user[0].Child_Date;
        $scope.appointmentList[i]["Child_Status"]=$scope.user[0].Child_Status;
        $scope.appointmentList[i]["Diagnostics"]=$scope.user[0].Diagnostics;
        $scope.appointmentList[i]["Documents"]=$scope.user[0].Documents;
        $scope.appointmentList[i]["History"]=$scope.user[0].History;
        $scope.appointmentList[i]["Kidoro_Code"]=$scope.user[0].Kidoro_Code;
        $scope.appointmentList[i]["Medication"]=$scope.user[0].Medication;
        $scope.appointmentList[i]["Procedures"]=$scope.user[0].Procedures;
        $scope.appointmentList[i]["Relationship_ID"]=$scope.user[0].Relationship_ID;
        $scope.appointmentList[i]["User_ID"]=$scope.user[0].Kidoro_Code;
        $scope.appointmentList[i]["User_Mail"]=$scope.user[0].User_Mail;
        $scope.appointmentList[i]["User_Name"]=$scope.user[0].User_Name;
        $scope.appointmentList[i]["User_Phone"]=$scope.user[0].User_Phone;
        $scope.appointmentList[i]["Vaccination"]=$scope.user[0].Vaccination;
        $scope.appointmentList[i]["Vitals"]=$scope.user[0].Vitals;
        $scope.appointmentList[i]["Gender"]=$scope.user[0].Gender;
        $scope.appointmentList[i]["Follow_Up"]=$scope.user[0].Follow_Up;
        $scope.appointmentList[i]["Child_Date"]=$scope.user[0].Child_Date;
        if($scope.appointmentList[i]["Child_Date"]!=null){
        var year = Number($scope.appointmentList[i]["Child_Date"].split("-")[2]);
        var today=new Date();
        $scope.appointmentList[i]["Age"]=today.getFullYear()-year;
        }

      }
      }
      }



    },function(err){

    });

    };
}

$scope.upateChanges = function(){
  $scope.updatedHospitalList = $scope.hospitalGroup;
}
$scope.showUpdates = function(){
  $scope.upateChanges();
  angular.element(updateDiv).css('height', 0 + "px");
  $scope.updateCount = null;
}
//Auto update
$scope.tem=0;
angular.element(updateDiv).css('height', 0 + "px");
 
$scope.showErrorMsg = false;
$scope.getAppointments = function(date){
  $ionicLoading.show({});
  $scope.showErrorMsg = false;
  var todayDate = new Date(date);
  var todayDateString = todayDate.getDate()+"-"+pad(todayDate.getMonth()+1)+"-"+todayDate.getFullYear();
  var query = new $kinvey.Query();
  query.equalTo('Doctor_ID', $scope.doctorId);
  var secondQuery = new $kinvey.Query();
  secondQuery.equalTo('Appointment_Date', todayDateString);
  var promise = $kinvey.DataStore.find('KidoroAppointment',query.and(secondQuery));
  promise.then(function(model) {
    $ionicLoading.hide();
    $scope.appointmentList=model;
    if ($scope.appointmentList.length == 0) {
      $scope.showErrorMsg = true;
      $scope.errorMsg = "No Appointment Exists till now";
    }
    $scope.populateUserDetails();
      $scope.groupHospitals();
    $scope.getConsultationFee();

    if($scope.previousRecordsLength==null){
      $scope.upateChanges();
    }

  },
 function(err) {
   $ionicLoading.hide();
});

}

$scope.getAppointments(todayDate);

 
  $scope.showCancel = false;

  $scope.cancelAppointment = function(itemVar) {
    $ionicLoading.show({});
    $scope.appointmentData = itemVar;
    $scope.user = userDataService.getData();

    var newaptRecords = {
    _id: $scope.appointmentData._id,
    Appointment_ID: $scope.appointmentData.Appointment_ID,
    Doctor_ID: $scope.appointmentData.Doctor_ID,
    Location_ID: $scope.appointmentData.Location_ID,
    User_ID: $scope.appointmentData.User_ID,
    Hospital_Name: $scope.appointmentData.Hospital_Name,
    Appointment_Time: $scope.appointmentData.Appointment_Time,
    Doctor_Name: $scope.appointmentData.Doctor_Name,
    Appointment_Date: $scope.appointmentData.Appointment_Date,
    Appointment_by: $scope.appointmentData.Appointment_by,
    Appointment_Time_Id: $scope.appointmentData.Appointment_Time_Id,
    Appointment_Status: "Cancelled",
    Coupon_Applied: $scope.appointmentData.Coupon_Applied,
    cancelled_by: $scope.user.Kidoro_Doc_Code
};

var promise = $kinvey.DataStore.save('KidoroAppointment',newaptRecords);
promise.then(function(model) {
  $scope.getAppointments($scope.selectedDate);
  $ionicLoading.hide();

}, function(err) {
  $ionicLoading.hide();
});

  }

$scope.saveAppointmentDetails = function(item) {
   passPatientDetails.setData(item);
}

function zeroPadding(n) {
              if(n<10)
              return ("000" + n);
              else if (n>9 && n<100)
              return ("00" + n);
              else if (n>99 && n<1000)
              return("0" + n);
              else return(n);
          }


function formatNumber(n){
    return n > 9 ? "" + n: "0" + n;
}


 var getKidoroTempCode = function(date,phNo){
  var monthList = ["Jan", "Feb", "March", "April", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
  var hashids = new Hashids((phNo).toString(), 5, "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890");
  var birthDate=date;
  var birthDateArr=birthDate.split("-");
  var month=formatNumber(Number(birthDateArr[1]));
  var dob=Number(birthDateArr[0].toString()+month.toString()+(Number(birthDateArr[2])%100).toString());

  var id = hashids.encode(dob);
  var code="TEMP"+id;
  return code;
  }


       $scope.addOfflineUser = function(tempCode){
                $scope.partner={kidoroCode:null};
                var totalCount;
                 var promise = $kinvey.DataStore.count('KidoroUser');
                 promise.then(function(response) {
                  $scope.totalCount = ++response;
                  $scope.count = "US"+zeroPadding($scope.totalCount);
                  var promise = $kinvey.DataStore.save('KidoroUser', {
                  User_ID:  $scope.count,
                  User_Name : $scope.newAppointmentData.User_Name,
                  User_Phone : Number($scope.newAppointmentData.User_Phone),
                  User_Mail : $scope.newAppointmentData.User_Mail,
                  Kidoro_Code :tempCode,
                  Child_Date : null,
                  Child_Due_Date: null,
                  Relationship_ID : $scope.partner,
                  Kidoro_Mom_Code : null,
                  Profile_Pic_URI : null,
                  User_Type:"Offline"

                  });
                  promise.then(function(model) {

                    $scope.user = model;
                  // send welcome mail on sign up
                    var html = "<p><b>Dear" + " " + $scope.user.User_Name + ",</b></p></br>" + "<p>Thank you for sign up with <b>Kidoro Health.</b></p>";
                    var text = "Congratulations! Welcome to Kidoro Health";
                    var subject = "Welcome to Kidoro Health";
                    var email = $scope.user.User_Mail;
                    var template_name = "welcome-email-kidoro";
                    var name = $scope.user.User_Name;
                  // send welcome mail on sign up


                  }, function(err) {
                    $ionicLoading.hide();

                         $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
        }, function (error) {
        });
                  });

},
function(error) {
                $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
        }, function (error) {
        });


});

  }
$scope.createOfflineAppointment=function(tempCode){



  var totalCount;
               var promise = $kinvey.DataStore.count('KidoroAppointment');
               promise.then(function(response) {
                $scope.totalCount = ++response;

              $scope.count = "AP"+zeroPadding($scope.totalCount);

              var promise = $kinvey.DataStore.save('KidoroAppointment', {

              Appointment_ID: $scope.count,
              Doctor_ID : $scope.user.Doctor_ID,
              Location_ID : $scope.selectedHospital.Location_ID,
              Appointment_by : $scope.user.Kidoro_Doc_Code,
              Hospital_Name : $scope.selectedHospital.hospitalName,
              Appointment_Time : $scope.timeString,
              Appointment_Time_Id: $scope.ampm,
              Doctor_Name : $scope.user.Doc_Name,
              Appointment_Date : $scope.dateString,
              User_ID : tempCode,
              Appointment_Status : "Confirmed",
              cancelled_by : null
             

     });


promise.then(function(model) {
  $scope.getAppointments($scope.selectedDate);
  $ionicLoading.hide();
  $scope.modal.hide();

}, function(err) {
  $ionicLoading.hide();
  $scope.modal.hide();
});


}, function(err) {
  $ionicLoading.hide();
});

}


 $scope.showConfirm = function(user) {
                $ionicLoading.hide();

              var confirmPopup = $ionicPopup.confirm({
                   title: "Kidoro",
                   template: '<b>'+user.User_Name+' </b> identified. </br> Do you want to continue?' ,
                   cssClass:'custom-popup',
                   buttons: [{
                    text: "No",
                    type: 'button-no',
                    onTap: function(e) {

                      confirmPopup.close();
                    }
                  }, {
                    text: "Yes",
                    type: 'button-yes',
                    onTap: function(e) {
                        $ionicLoading.show();
                        $scope.createOfflineAppointment(user.Kidoro_Code);

                      }
                  }]
                });
                };

$scope.addAppointment=function(){



  $ionicLoading.show();

  if($scope.newAppointmentData.User_Name.length>0&&$scope.newAppointmentData.User_Phone!=null&&$scope.newAppointmentData.User_Mail.length>0&&$scope.newAppointmentData.Appointment_Date.length>0)
{
  $scope.user = userDataService.getData();
var date = new Date($scope.newAppointmentData.Appointment_Date);

var dateString=date.getDate()+"-"+pad(date.getMonth()+1)+"-"+date.getFullYear();


                var query = new $kinvey.Query();
                query.equalTo('User_Phone',  Number($scope.newAppointmentData.User_Phone));
                var promise = $kinvey.DataStore.find('KidoroUser', query);

                promise.then(function(models) {



                  $ionicLoading.hide();

                  if(models.length!=0){
                    
                     $scope.showConfirm(models[0]);
                  }
                  else{
                    var tempKidoroCode = getKidoroTempCode(dateString,Number($scope.newAppointmentData.User_Phone));
                   
                    $scope.addOfflineUser(tempKidoroCode);
                    $scope.createOfflineAppointment(tempKidoroCode);


                  
                  }
                    }, function(err) {
                  $ionicLoading.hide();
                 $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
        }, function (error) {
        });
     });
  }
  else{
      var errorAlert = $ionicPopup.alert({
                                    title: 'Kidoro Health',
                                    template: "Please enter the required Fields",
                                    cssClass:'custom-popup-alert',
                                     buttons: [{
                                     text: "Ok",
                                     type: 'button-alert-ok',
                                     onTap: function(e) {
                                       errorAlert.close();


                                     }
                                   }]
                                 });
        }
     }


$scope.createHospitalNameList();
$ionicModal.fromTemplateUrl('templates/addAppointmentModal.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });
  
    $scope.openDatePicker = function() {
    $scope.tmp = {};
    $scope.tmp.newDate = $scope.newAppointmentData.Appointment_Date;

    var birthDatePopup = $ionicPopup.show({
     template: '<datetimepicker ng-model="tmp.newDate"></datetimepicker>',
     title: "Appointment Date",
     scope: $scope,
     buttons: [
       { text: 'Cancel' },
       {
         text: '<b>Save</b>',
         type: 'button-positive',
         onTap: function(e) {
         
           var tempDate = new Date($scope.tmp.newDate);
           $scope.dateString = tempDate.getDate()+"-"+pad(tempDate.getMonth()+1)+"-"+tempDate.getFullYear();
           $scope.timeString = tempDate.getHours()+":"+tempDate.getMinutes();
           $scope.ampm = (tempDate.getHours() >= 12) ? "PM" : "AM";
           $scope.newAppointmentData.Appointment_Date = $scope.dateString+" "+$scope.timeString+" "+$scope.ampm;
         }
       }
     ]
    });
  }
  $scope.newAppointmentData = {
  
              Doctor_ID :null,
              Location_ID :null,
              Appointment_by : null,
              Hospital_Name : null,
              Appointment_Time :null,
              Appointment_Time_Id: null,
              Doctor_Name : null,
              Appointment_Date :"",
              User_Name:"",
              User_Phone:null,
              User_Mail:"",
              Appointment_Status : "Confirmed",
              cancelled_by : null
};
}catch(err){
      $ionicLoading.hide();
      $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
      }, function (error) {
      });
  }
})




.controller('saveMedicalRecordCtrl', function($scope, $state, $cordovaToast,passPrescriptionData, $ionicHistory, userDataService, $ionicModal, $ionicPopup ,$kinvey, $ionicLoading, passPatientDetails, passLocationDetails, $cordovaCamera) {


try{


  function pad(d) {
    return (d < 10) ? '0' + d.toString() : d.toString();
}

  $scope.images = [];
  $scope.blobImages = [];
  $scope.imageArray = [];
  $scope.prescriptionBlobArray = [];
  $scope.PrescriptionImagesArray = [];
  $scope.metadata=[];

  $scope.user = userDataService.getData();
  $scope.appointmentData = passPatientDetails.getData();
  $scope.title = $scope.appointmentData.User_Name;
  $scope.procedure={name:"",price:0};
  $scope.totalPrice=Number($scope.appointmentData.Consultaion_Fee[0].firstVisit)+Number($scope.procedure.price);

  $scope.FollowUpList=[
      {followUp:'1 Day', noOfDays:1},
      {followUp:'2 Days', noOfDays:2},
      {followUp:'3 Days', noOfDays:3},
      {followUp:'1 Week', noOfDays:7},
      {followUp:'2 Weeks', noOfDays:14}
    ];
    $scope.followUp=$scope.FollowUpList[0];

$scope.folloUpChanged=function(object){
$scope.selectedFollowUp=object.followUp;
}

$scope.inputs = [{
      Procedure_Name: null,
      Price: null
  }];

  $scope.addInput = function () {
      $scope.inputs.push({
          Procedure_Name: null,
          Price: null
      });
  }

  $scope.removeInput = function (index) {
      $scope.inputs.splice(index, 1);
  }
$scope.changePrice=function(){
  var procedureSum=0;
  for (var i = $scope.inputs.length - 1; i >= 0; i--) {
    procedureSum=procedureSum+Number($scope.inputs[i].Price)
  };
$scope.totalPrice=Number($scope.appointmentData.Consultaion_Fee[0].firstVisit)+procedureSum;

}

$scope.selectedFollowUp=$scope.FollowUpList[0].followUp;


  // Camera Code
    $scope.takePhoto = function () {
                var options = {
                  quality: 20,
                  destinationType: Camera.DestinationType.DATA_URL,
                  sourceType: Camera.PictureSourceType.CAMERA,
                  allowEdit: false,
                  encodingType: Camera.EncodingType.JPEG,
                  saveToPhotoAlbum: false,
                  correctOrientation: true
              };

                  $cordovaCamera.getPicture(options).then(function (imageData) {
                    if (imageData !== null || imageData !== undefined) {
                      $scope.imageArray.push({'image': "data:image/jpeg;base64," + imageData});
                      $scope.blobImages.push({'image': imageData});
                      $scope.images.push({'type': 'image/png', 'name':'Prescription_Image', 'content':imageData});
                 }

                  });
              }
  
      $scope.saveMedicalRecord = function() {

        var jsonArray=[];
        for (var i = $scope.inputs.length - 1; i >= 0; i--) {
          jsonArray.push({
            "Procedure_Name": $scope.inputs[i].Procedure_Name,
                "Price":  $scope.inputs[i].Price
          });
        };

        var tempJsonArray = [];

        var tempJsonArray = angular.copy(jsonArray);
        $scope.test = jsonArray;

        var currentTime = new Date();
        var uniqueDateTime = currentTime.getTime();
        var confirmPopup = $ionicPopup.confirm({
          title: 'Review Data',
          template: '<div class="row"><span class="col col-66">Follow Up</span><span class="col col-33">{{selectedFollowUp}}</span></div><div class="row"><span class="col col-66">Consultaion Fee</span><span class="col col-33">&#8377 {{appointmentData.Consultaion_Fee[0].firstVisit}}</span></div><div class="row" ng-repeat = "item in test"><span class="col col-66" ng-show="item.Procedure_Name.length">{{item.Procedure_Name}}</span><span class="col col-33" ng-show="item.Price.length">&#8377 {{item.Price}}</span></div>',
          scope: $scope,
        });
        confirmPopup.then(function(res) {
          if(res) {


                                      var todayDate=new Date();
                                      var medRecord = {
                                      Medical_ID : 'tempId',
                                      Doctor_ID : $scope.appointmentData.Doctor_ID,
                                      Location_ID : $scope.appointmentData.Location_ID,
                                      Kidoro_Code : $scope.appointmentData.Kidoro_Code,
                                      Follow_Up : $scope.selectedFollowUp,
                                      Consultation_Fee : $scope.appointmentData.Consultaion_Fee[0].firstVisit,
                                      Total_Fee : $scope.totalPrice,
                                      Record_Date : pad(todayDate.getDate())+"-"+pad(todayDate.getMonth()+1)+"-"+todayDate.getFullYear(),
                                      Procedure_ID : tempJsonArray,
                                      pres_id: uniqueDateTime
                                  };

            $ionicLoading.show({});
            var contentType = 'image/png';

          if ($scope.blobImages.length > 0) {
            for (var i = 0; i < $scope.blobImages.length; i++) {
             var b64Data = $scope.blobImages[i].image;
             var blob = b64toBlob(b64Data, contentType);
             $scope.prescriptionBlobArray.push(blob);
           }

          }
          if ($scope.prescriptionBlobArray.length > 0) {
            for (var i = 0; i < $scope.prescriptionBlobArray.length; i++) {

                 var fileContent = $scope.prescriptionBlobArray[i];
                  var data = {
                                  _filename : uniqueDateTime,
                                  mimeType  : 'image/png',
                                  size      : fileContent.length
                                }
                                $scope.metadata.push(data);
                                 var promise = $kinvey.File.upload(fileContent, $scope.metadata[i], {
                                     public:true
                                 });
                                 promise.then(function(response) {

                                      var promise = $kinvey.File.stream(response._id);
                                       promise.then(function(file) {
                                           var url = file._downloadURL;
                                           $scope.PrescriptionImagesArray.push({'pres_link': url, 'pres_name': response._filename});


                                       if($scope.metadata.length==$scope.prescriptionBlobArray.length){

                                          if(response._filename==$scope.metadata[$scope.prescriptionBlobArray.length-1]._filename){
                                            $scope.PrescriptionData = {
                                                                        pres_link_array: $scope.PrescriptionImagesArray,
                                                                      };
                                                                      passPrescriptionData.setData($scope.PrescriptionData);
                                                                      passLocationDetails.setData(medRecord);
                                                                      $ionicLoading.hide();
                                                                      $state.go('menu.savedMedicalRecord');
                                          }
                                        }

                                       });
                                 }, function(err) {
                                     $ionicLoading.hide();

                                 });

               }
          } else {
            $scope.PrescriptionData = null;
            passPrescriptionData.setData($scope.PrescriptionData);
            passLocationDetails.setData(medRecord);
            $ionicLoading.hide();
            $state.go('menu.savedMedicalRecord');
          }

          } else {
          }
        })
  }

  // base64 string to blob

                            function b64toBlob(b64Data, contentType, sliceSize) {
                            contentType = contentType || '';
                            sliceSize = sliceSize || 512;

                            var byteCharacters = atob(b64Data);
                            var byteArrays = [];

                            for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
                                var slice = byteCharacters.slice(offset, offset + sliceSize);

                                var byteNumbers = new Array(slice.length);
                                for (var i = 0; i < slice.length; i++) {
                                    byteNumbers[i] = slice.charCodeAt(i);
                                }

                                var byteArray = new Uint8Array(byteNumbers);

                                byteArrays.push(byteArray);
                            }

                            var blob = new Blob(byteArrays, {type: contentType});
                            return blob;
                        }

       // base64 string to blob
}catch(err){
      $ionicLoading.hide();
      $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
      }, function (error) {
      });
  }
})

.controller('savedMedicalRecordCtrl', function($scope, $state, $cordovaToast, $kinvey, passPrescriptionData, $ionicLoading, $ionicHistory, passPatientDetails, userDataService, passLocationDetails, passDoctorDetails) {
 

 try{



  $scope.presData = passPrescriptionData.getData();
  $scope.appointmentData = passPatientDetails.getData();
  $scope.medicalRecord = passLocationDetails.getData();
  $scope.doctorDetails = passDoctorDetails.getData();
  $scope.user = userDataService.getData();
if ($scope.presData == null) {

  var prescriptionRecord = {
  pres_id: null,
  pres_image_array: null,
  pres_uploaded_by: $scope.user.Kidoro_Doc_Code
};

} else {

    var prescriptionRecord = {
    pres_id: Number($scope.presData.pres_link_array[0].pres_name),
    pres_image_array: $scope.presData.pres_link_array,
    pres_uploaded_by: $scope.user.Kidoro_Doc_Code,
    pres_uploader_name: $scope.user.Doc_Name
  };

}

      var aptRecords = {
      _id: $scope.appointmentData._id,
      Appointment_ID: $scope.appointmentData.Appointment_ID,
      Doctor_ID: $scope.appointmentData.Doctor_ID,
      Location_ID: $scope.appointmentData.Location_ID,
      User_ID: $scope.appointmentData.User_ID,
      Hospital_Name: $scope.appointmentData.Hospital_Name,
      Appointment_Time: $scope.appointmentData.Appointment_Time,
      Doctor_Name: $scope.appointmentData.Doctor_Name,
      Appointment_Date: $scope.appointmentData.Appointment_Date,
      Appointment_by: $scope.appointmentData.Appointment_by,
      Appointment_Time_Id: $scope.appointmentData.Appointment_Time_Id,
      Appointment_Status: "Visited",
      Coupon_Applied: $scope.appointmentData.Coupon_Applied,
      cancelled_by: $scope.appointmentData.cancelled_by

  };
          $ionicLoading.show({});
          var promise = $kinvey.DataStore.save('KidoroMedicalRecords', $scope.medicalRecord);
          promise.then(function(model) {

            var promise = $kinvey.DataStore.save('KidoroPrescriptions', prescriptionRecord);
            promise.then(function(model) {

              var promise = $kinvey.DataStore.save('KidoroAppointment', aptRecords);
              promise.then(function(model) {
                $ionicLoading.hide();

              }, function(err) {
                  $ionicLoading.hide();
              });

            }, function(err) {
                $ionicLoading.hide();
            });

          }, function(err) {
              $ionicLoading.hide();
          });

$scope.goToAppointment = function() {
  $ionicHistory.nextViewOptions({
        disableAnimate: true,
        disableBack: true
       });
     $state.go('menu.appointments');
  }

 }catch(err){
      $ionicLoading.hide();
      $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
      }, function (error) {
      });
  }
})



.controller('patientCtrl', function($scope, $ionicScrollDelegate, $cordovaToast, $ionicLoading,patientListService, $cordovaToast,$state,$kinvey,sendSMSServices,passPatientDetails,userDataService) {


try{


  function contains(a, obj) {
    var i = a.length;
    while (i--) {
       if (a[i] === obj) {
           return true;
       }
    }
    return false;
}

  $scope.sendMessage = function(item) {
    passPatientDetails.setData(item);
    $state.go('menu.chat');
  }
    $scope.sendInvite = function(item){
   
    $cordovaToast.show("Invite Sent", 1500, "bottom").then(function(success) {
        }, function (error) {
        });
    var number = item.User_Phone;
    var message = $scope.user.Doc_Name+" has invited you to join his Kidoro Network. Download kidoroHealth to get all your medical updates. bit.ly/1QL1sCC"; //Add download url
    sendSMSServices.getRespose(number, message);
  }

  $scope.patientDetails = function(item) {
    passPatientDetails.setData(item);
    $state.go('menu.patientDetails');
  }


  $scope.patientsList=patientListService.getData();
  $scope.user=userDataService.getData();
  $scope.doctorId =$scope.user.Doctor_ID ;

  $scope.clearSearch = function() {
    $scope.search = '';
  };

 }catch(err){
      $ionicLoading.hide();
      $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
      }, function (error) {
      });
  }
})

.controller('patientDetailsCtrl', function($scope, $ionicLoading, $cordovaToast, $ionicPopup, $kinvey, $ionicModal, userDataService,passPatientDetails, passDoctorDetails) {

 try{


  $ionicLoading.show({});

  $scope.patientData=passPatientDetails.getData();
  $scope.title = $scope.patientData.User_Name;

 var query = new Kinvey.Query();
  query.equalTo('Kidoro_Code', $scope.patientData.Relationship_ID.kidoroCode);
var promise = $kinvey.DataStore.find('KidoroUser',query);
  promise.then(function(model) {

  $scope.relationships=model[0];
  $ionicLoading.hide();

  },function(err){
           // alert('Failed '+err.description);
             $ionicLoading.hide();
    });

  $ionicModal.fromTemplateUrl('templates/patientHistory.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
      $scope.modal = modal;
    });

    $scope.openModal = function(uName) {
      $scope.title = uName;
      if ($scope.length == 0) {
        $ionicPopup.alert({
             title: 'MEDICAL RECORDS',
             template: '<center>No record added previously</center>'
           });
      } else {
        $scope.modal.show();
      }
    };

    // JSON object for Report
    $scope.patientData=passPatientDetails.getData();
    $scope.doctorDetails = passDoctorDetails.getData();
    $scope.doctorId = userDataService.getData().Doctor_ID;
    $scope.medicalRecordArray=[];

     var query = new Kinvey.Query();
      query.equalTo('Kidoro_Code', $scope.patientData.Kidoro_Code);
      var querySecond = new Kinvey.Query();
      querySecond.equalTo('Doctor_ID', $scope.doctorId);
    var promise = $kinvey.DataStore.find('KidoroMedicalRecords',query.and(querySecond));
      promise.then(function(model) {
        var length;
        $scope.length = model.length;
      },function(err){
               // alert('Failed '+err.description);
        });

  }catch(err){
      $ionicLoading.hide();
      $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
       
      }, function (error) {
      });
  }
  
})


.controller("patientHistoryModalCtrl", function($scope,$kinvey,$ionicLoading, $cordovaToast,passPatientDetails,userDataService,passDoctorDetails){

try{

        $ionicLoading.show({});
        $scope.patientData=passPatientDetails.getData();
        $scope.doctorDetails = passDoctorDetails.getData();
        $scope.doctorId =userDataService.getData().Doctor_ID;
        $scope.medicalRecordArray=[];

         var query = new Kinvey.Query();

          query.equalTo('Kidoro_Code', $scope.patientData.Kidoro_Code);
          var querySecond = new Kinvey.Query();
          querySecond.equalTo('Doctor_ID', $scope.doctorId);
        var promise = $kinvey.DataStore.find('KidoroMedicalRecords',query.and(querySecond));
          promise.then(function(model) {
            var length;
            $scope.length = model.length;
            if ($scope.length == 1) {
              $scope.hideNext = true;
              $scope.hidePrev = true;
            }
          $scope.medicalRecordArray = model;
          for (var i = 0; i < $scope.medicalRecordArray.length; i++) {
          var query = new Kinvey.Query();
          query.equalTo('Location_ID', $scope.medicalRecordArray[i].Location_ID);
          var promise = $kinvey.DataStore.find('KidoroLocation',query);
          promise.then(function(model) {
            for (var j = 0; j < $scope.medicalRecordArray.length; j++){
              if($scope.medicalRecordArray[j].Location_ID==model[0].Location_ID){
                  $scope.medicalRecordArray[j]["Hospital_Name"]=model[0].Hospital_Name;
                  $scope.medicalRecordArray[j]["Locality"]=model[0].Locality;
              }
            }
            $scope.medicalRecord=$scope.medicalRecordArray[$scope.medicalRecordArray.length-1];
            $ionicLoading.hide();
          },function(err){
              $ionicLoading.hide();
                   // alert('Failed '+err.description);
            });

          };

          },function(err){
              $ionicLoading.hide();
                   // alert('Failed '+err.description);
            });

        $scope.hideNext = true;
        $scope.hidePrev = false;

        $scope.next=function(){
        var pos = $scope.medicalRecordArray.indexOf($scope.medicalRecord);

        if(pos==$scope.medicalRecordArray.length-2)
        {
          $scope.hideNext = true;
          $scope.hidePrev = false;
        } else if(pos==0){
          $scope.hideNext = false;
          $scope.hidePrev = false;
        }

        if(pos<$scope.medicalRecordArray.length-1){
          pos++;
          $scope.medicalRecord=$scope.medicalRecordArray[pos];
        }

        }

        $scope.previous=function(){
        var pos = $scope.medicalRecordArray.indexOf($scope.medicalRecord);

        if(pos==$scope.medicalRecordArray.length-1)
        {
          $scope.hideNext = false;
          $scope.hidePrev = false;
        } else if(pos==1) {
          $scope.hideNext = false;
          $scope.hidePrev = true;
        }

        if(pos>0){
          pos--;
          $scope.medicalRecord=$scope.medicalRecordArray[pos];
        }

        }

 }catch(err){
      $ionicLoading.hide();
      $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
      }, function (error) {
      });
  }
// JSON object for Report


})

.controller("doctorClinicDetailsCtrl", function($scope, $state, $ionicLoading, $cordovaToast, $ionicPopover,$kinvey,userDataService,passPatientDetails,passLocationDetails){

try{

          $ionicLoading.show();
          $scope.user=userDataService.getData();
          $scope.doctorId =$scope.user.Doctor_ID;

          var query = new Kinvey.Query();
            query.equalTo('Doctor_ID', $scope.doctorId);
            var promise = $kinvey.DataStore.find('KidoroDocs',query);
            promise.then(function(model) {
             $scope.doctorDetails=model[0];
             $scope.qualification=[];

             if ($scope.doctorDetails.Qualification!==null) {
              $scope.doctorDetails["qual"] = $scope.doctorDetails.Qualification.replace("^", ', ');
             }
             if ($scope.doctorDetails.Specialization !== null) {
               $scope.doctorDetails.Specialization=$scope.doctorDetails.Specialization.replace("^", ', ');
             }

             if ($scope.doctorDetails.Doc_Pic==null) {
               $scope.doctorDetails.Doc_Pic = "./img/noimage.png";
             }

          if($scope.doctorDetails.Location_ID==null || $scope.doctorDetails.Location_ID==='undefined'){
          var locationArray=[];
           $ionicLoading.hide();
          }
          else{
            var locationArray=$scope.doctorDetails.Location_ID.split(",");
          }

             $scope.LocationDataArray = [];
             for (var i = locationArray.length - 1; i >= 0; i--) {
            var query = new Kinvey.Query();
            query.equalTo('Location_ID', locationArray[i]);
            var promise = $kinvey.DataStore.find('KidoroLocation',query)
            promise.then(function(model) {

              $scope.LocationDataArray.push(model[0]);
                 $ionicLoading.hide();
            },function(err){
               $ionicLoading.hide();
                     // alert('Failed '+err.description);
              });
             };


            },function(err){
              $ionicLoading.hide();
                     // alert('Failed '+err.description);
              });
            $ionicPopover.fromTemplateUrl('templates/popoverEditProfile.html', {
              scope: $scope,
            }).then(function(popover) {

              $scope.popover = popover;
            });
            $ionicPopover.fromTemplateUrl('templates/editPopover.html', {
              scope: $scope,
            }).then(function(popover) {
              $scope.popoveredit = popover;
            });

            $scope.signOut=function(){
              $ionicLoading.show();
              var promise = Kinvey.User.logout();
              promise.then(function() {
                $ionicLoading.hide();
                  $state.go('loginHome');
              }, function(error) {

              });

            }
          $scope.popovereditclicked=function(event,location) {
            $scope.location=location;
           $scope.hgt=event.pageY+5;
            angular.element( document.querySelector( '#editPopover' ) ).css('top',event.pageY+"px!important");
            $scope.popoveredit.show();
          }

            $scope.div1 = true;
              angular.element( document.querySelector( '#btn_mom' ) ).css('border-bottom-color','#faac1c');
               angular.element( document.querySelector( '#btn_mom' ) ).css('color','#000');
              $scope.showMom = function(){
                $scope.div1 = true;
                $scope.div2 = false;
                $scope.div3 = false;
                angular.element( document.querySelector( '#btn_mom' ) ).css('border-bottom-color','#faac1c');
                angular.element( document.querySelector( '#btn_mom' ) ).css('color','#000');
                angular.element( document.querySelector( '#btn_kid' ) ).css('border-bottom-color','#fff');
                angular.element( document.querySelector( '#btn_kid' ) ).css('color','#ccc');
                angular.element( document.querySelector( '#btn_family' ) ).css('border-bottom-color','#fff');
              };

              $scope.showKidoro = function(){
                $scope.div1 = false;
                $scope.div2 = true;
                $scope.div3 = false;
                angular.element( document.querySelector( '#btn_mom' ) ).css('border-bottom-color','#fff');
                angular.element( document.querySelector( '#btn_mom' ) ).css('color','#ccc');
                angular.element( document.querySelector( '#btn_kid' ) ).css('border-bottom-color','#faac1c');
                angular.element( document.querySelector( '#btn_kid' ) ).css('color','#000');
                angular.element( document.querySelector( '#btn_family' ) ).css('border-bottom-color','#fff');
              };
            
                  $scope.divedit = true;
                  $scope.mom = true;
                  $scope.baby = true;

                $scope.showEdit = function() {

                $scope.momName = $scope.user.Qualification;
                $scope.momEmail = $scope.user.Specialization;
                $scope.babyName = $scope.user.experience;

                $scope.divdone=true;
                $scope.divedit = false;
                $scope.mom = false;

                angular.element( document.querySelector( '#mom_itm0' ) ).css('color','#666');
                angular.element( document.querySelector( '#mom_itm1' ) ).css('color','#009999');
                angular.element( document.querySelector( '#mom_itm2' ) ).css('color','#666');
                angular.element( document.querySelector( '#mom_itm3' ) ).css('color','#009999');



                $scope.baby = false;
                angular.element( document.querySelector( '#kid_itm0' ) ).css('color','#666');
                angular.element( document.querySelector( '#kid_itm1' ) ).css('color','#009999');
                angular.element( document.querySelector( '#kid_itm2' ) ).css('color','#666');

                $scope.partner = false;
                angular.element( document.querySelector( '#partner_itm0' ) ).css('color','#666');
                angular.element( document.querySelector( '#partner_itm1' ) ).css('color','#666');
                angular.element( document.querySelector( '#partner_itm2' ) ).css('color','#666');

              };

            $scope.editDrProfile = function() {
              passPatientDetails.setData($scope.doctorDetails);
              $state.go('menu.editDrProfile');
            }

            // edit clinic click
            $scope.editClinic = function() {
               $scope.popoveredit.hide();

              passPatientDetails.setData($scope.doctorDetails);
              passLocationDetails.setData($scope.location);
              $state.go('menu.editClinic');
            }
            // edit clinic click

            $scope.addClinic = function() {
              passPatientDetails.setData($scope.doctorDetails);
              $state.go('menu.addClinic');
            }




          }catch(err){
                $ionicLoading.hide();
                $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
                }, function (error) {
                });
            }
})

.controller("editClinicCtrl", function($state, $scope,$kinvey, $ionicModal, $cordovaToast,$ionicLoading, $ionicHistory,passPatientDetails,passLocationDetails){

try{


               function makeDaysString(binaryArray){
                var daysArray=["MON","TUE","WED","THU","FRI","SAT","SUN"];
                var dayStringArray=[];

                for (var i = 0; i <binaryArray.length; i++) {
                  if(binaryArray[i]==1){

                    for (var j = i+1; j <=binaryArray.length; j++) {
                      if(binaryArray[j]==0 || binaryArray[j]==undefined){
                        if(daysArray[i]!=daysArray[j-1]){
                           dayStringArray.push({days:daysArray[i]+"-"+daysArray[j-1]});
                        }
                        else{
                          dayStringArray.push({days:daysArray[i]});
                        }

                        i=j;
                        break;
                      }
                    };
                  }

                };

                var dayString="";
              for (var i = 0; i < dayStringArray.length; i++) {
                if(dayString.length>0){
                  dayString=dayString.concat(",",dayStringArray[i].days);
                }
                else{
                  dayString=dayString.concat(dayStringArray[i].days);
                }
                }
                return dayString;
              }

              function returnTimeString(timingArray){
                var timeString="";
              for (var i = 0; i < timingArray.length; i++) {
                if(timeString.length>0){
                  timeString=timeString.concat(" | ",timingArray[i].fromTime+"-"+timingArray[i].toTime);
                }
                else{
                  timeString=timeString.concat(timingArray[i].fromTime+"-"+timingArray[i].toTime);
                }
                }
                return timeString;
              }
              $scope.doctorDetails=passPatientDetails.getData();
              $scope.LocationData=passLocationDetails.getData();

                if(typeof $scope.LocationData.Services === 'undefined' ||  $scope.LocationData.Services==null){
                $scope.servicesSelected=[];
              }else{
                $scope.servicesSelected=$scope.LocationData.Services;
              }

              $scope.dayTimingArray=$scope.LocationData.Timing;

              for (var i = 0; i < $scope.dayTimingArray.length; i++) {
                $scope.dayTimingArray[i]["timeString"]=returnTimeString($scope.dayTimingArray[i].timing);
              };


             $scope.inputs = [{
                    fromTime: "From Time",
                    toTime: "To Time"
                }];

                $scope.addInput = function () {
                    $scope.inputs.push({
                        fromTime: "From Time",
                        toTime: "To Time"
                    });
                }

                $scope.removeInput = function (index) {
                    $scope.inputs.splice(index, 1);
                }

            $scope.timeSlotChange=function(timeSlot){
            $scope.timeSlotSelected=timeSlot.time;
            }
              // Date Time Picker
            $scope.slots = {epochTime: 12600, format: 12, step: 15};
            function prependZero(param) {
            if (String(param).length < 2) {
               return "0" + String(param);
                }
                  return param;
            }

             function epochParser(val, opType) {
              if (val === null) {
                return "00:00";
              } else {
                var meridian = ['AM', 'PM'];

                  if (opType === 'time') {
                    var hours = parseInt(val / 3600);
                    var minutes = (val / 60) % 60;
                    var hoursRes = hours > 12 ? (hours - 12) : hours;

                    var currentMeridian = meridian[parseInt(hours / 12)];

                    return (prependZero(hoursRes) + ":" + prependZero(minutes) + " " + currentMeridian);
                  }
                }
            }

            $scope.fromTime=epochParser($scope.slots.epochTime,'time');
            $scope.fromTimePickerCallback = function (val) {
              if (typeof (val) === 'undefined') {
              } else {
                $scope.fromTime=epochParser(val,'time');
                $scope.toTime=epochParser(val+900,'time');

                    $scope.inputs[$scope.index].fromTime=$scope.fromTime;
              }
            };

            $scope.toTime=epochParser($scope.slots.epochTime,'time');
            $scope.toTimePickerCallback = function (val) {
              if (typeof (val) === 'undefined') {
              } else {
                $scope.toTime=epochParser(val,'time');

                    $scope.inputs[$scope.index].toTime=$scope.toTime;
              }
            };
            $scope.getIndex=function(index){
              $scope.index=index;
            }
                $scope.dayList = [
                {"day":"Monday",checked:false},
                {"day":"Tuesday",checked:false},
                {"day":"Wednesday",checked:false},
                {"day":"Thursday",checked:false},
                {"day":"Friday",checked:false},
                {"day":"Saturday",checked:false},
                {"day":"Sunday",checked:false},
                ]


                 $scope.timeSlotArray=[{
                  time:"15 Min",id:1},{
                  time:"30 Min",id:2},{
                  time:"45 Min",id:3},{
                  time:"1 Hour",id:4},{
                  time:"2 Hour",id:5}
                ];

                $scope.timeSlotSelected=$scope.timeSlotArray[0].time;

              $scope.consultationFees=[];
              $scope.firstVisitFee={firstVisit:$scope.LocationData.Consultation_Fee[0].firstVisit}
              $scope.secondVisitFee={secondVisit:null};

                $scope.timeSlot=$scope.timeSlotArray[0];

              $scope.goToDoctorClinic = function(){

              $ionicLoading.show({
                content: 'Updating',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
              });

                    $ionicHistory.nextViewOptions({
                    disableAnimate: true,
                    disableBack: true
                  });


                    jsonServicesArray=[];
                for (var i = $scope.servicesSelected.length - 1; i >= 0; i--) {
                  jsonServicesArray.push({
                    "name": $scope.servicesSelected[i].name
                  });
                };

                var jsonTimingArray=[];
                for (var i = $scope.dayTimingArray.length - 1; i >= 0; i--) {
                  jsonTimingArray.push({
                    "days": $scope.dayTimingArray[i].days,
                    "timing":$scope.dayTimingArray[i].timing,
                    "time_slot":$scope.dayTimingArray[i].time_slot,
                    "binaryArray":$scope.dayTimingArray[i].binaryArray
                  });
                };
                 $scope.consultationFees.push($scope.firstVisitFee);
                $scope.consultationFees.push($scope.secondVisitFee);
             var entity ={
                _id: $scope.LocationData.LocationUpdateId,
                Address: $scope.LocationData.Address,
                Consultation_Fee: $scope.consultationFees,
                Default_Location: $scope.LocationData.Default_Location,
                Doctor_ID: $scope.LocationData.Doctor_ID,
                Hospital_Name: $scope.LocationData.Hospital_Name,
                Hospital_Phone: Number($scope.LocationData.Hospital_Phone),
                Locality: $scope.LocationData.Locality,
                Location_ID: $scope.LocationData.Location_ID,
                Timing: jsonTimingArray,
                Services:jsonServicesArray
                   }
                   var acl    = new $kinvey.Acl(entity);
                  var promise = $kinvey.DataStore.save('KidoroLocation', entity);
            promise.then(function(model) {
                $ionicLoading.hide();
                 $state.go('menu.doctorClinicDetails');

            }, function(err) {
                $ionicLoading.hide();
                // alert(err);
            });

                };

              $scope.serviceList = [];

              var promise = Kinvey.DataStore.get('KidoroDocServices');
              promise.then(function(entity) {
                  for (var i = 0; i < entity.length; i++) {
                    $scope.serviceList.push({'name': entity[i].Service_Name});
                  }
              }, function(error) {
              });


              $ionicModal.fromTemplateUrl('templates/servicesListModal.html', {
                scope: $scope,
                   animation: 'slide-in-up',
                   backdropClickToClose: false,
                   hardwareBackButtonClose: false,
                   focusFirstInput: true
              }).then(function(serviceModal) {
               $scope.serviceModal = serviceModal;
               $scope.serviceModal.searchText = "";
              });

                $scope.clearSearch = function() {
                  $scope.serviceModal.searchText = "";
                }

              $scope.openServiceModal = function() {
                   for (var i = 0; i < $scope.serviceList.length; i++) {
                    for (var j = 0; j<$scope.servicesSelected.length; j++) {
                      if($scope.servicesSelected[j].name==$scope.serviceList[i].name){
                        $scope.serviceList[i]["checked"]=true;
                      }

                    };

              }

              $scope.serviceModal.show();
              }


                $scope.servicesDone=function(){

                $scope.servicesSelected=[];
                for (var i = 0; i < $scope.serviceList.length; i++) {
                if($scope.serviceList[i].checked==true){
                $scope.servicesSelected.push($scope.serviceList[i]);
                }
              }
               $scope.serviceModal.hide();
              }

                $scope.closeServiceModal = function() {
                  $scope.serviceModal.hide();
                }

                $scope.$on('$destroy', function() {
                   $scope.serviceModal.remove();
                 });

              $ionicModal.fromTemplateUrl('templates/consultationHoursModal.html', {
               scope: $scope,
               animation: 'slide-in-up'
             }).then(function(consultationHoursModal) {
               $scope.consultationHoursModal = consultationHoursModal;
             });

            $scope.editconsultationHours = function(timing,index) {
              $scope.editIndex=index;
               $scope.inputs = [{
                    fromTime: timing.timing[0].fromTime,
                    toTime: timing.timing[0].toTime
                }];
              for (var i = 0; i <timing.binaryArray.length; i++) {
                if(timing.binaryArray[i]==1){
                $scope.dayList[i].checked=true;
                }
                else{
                $scope.dayList[i].checked=false;
                }
              };
              $scope.consultationHoursModal.show();
            }

            $scope.addconsultationHours = function() {
              $scope.editIndex=null;
               $scope.inputs = [{
                    fromTime: "From Time",
                    toTime: "To Time"
                }];
                for (var i = 0; i <$scope.dayList.length; i++) {
                $scope.dayList[i].checked=false;
              };
              $scope.consultationHoursModal.show();
            }

            $scope.consultationHoursDone=function(){
               $scope.newTimeSelected={
                  days:null,
                  timing:null,
                  time_slot:null,
                  binaryArray:null,
                  timeString:null
                };
                $scope.tempBinaryArray=[];
              for (var i = 0; i <$scope.dayList.length; i++) {
                if($scope.dayList[i].checked==true){
                  $scope.tempBinaryArray.push(1);
                }
                else{
                  $scope.tempBinaryArray.push(0);
                }
              };
              var dayString=makeDaysString($scope.tempBinaryArray);
              var timeString=returnTimeString($scope.inputs);
              $scope.newTimeSelected.days=dayString;
              $scope.newTimeSelected.timing=$scope.inputs;
              $scope.newTimeSelected.time_slot=$scope.timeSlotSelected;
              $scope.newTimeSelected.binaryArray=$scope.tempBinaryArray;
              $scope.newTimeSelected.timeString=timeString;
              if($scope.editIndex!=null){
                $scope.dayTimingArray[$scope.editIndex]=$scope.newTimeSelected;
              }
              else{
              $scope.dayTimingArray.push($scope.newTimeSelected);
              }


              $scope.consultationHoursModal.hide();
            }

            $scope.closeconsultationHours = function() {
              $scope.consultationHoursModal.hide();
            }

            $scope.$on('$destroy', function() {
               $scope.consultationHoursModal.remove();
             });

        }catch(err){
                $ionicLoading.hide();
                $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
                }, function (error) {
                });
            }
})


.controller("addClinicCtrl", function($state, $scope,$kinvey, $cordovaToast, $ionicModal,$ionicLoading, $ionicHistory,passPatientDetails,passLocationDetails){

try{

    $scope.clinic = {
        Hospital_Name:null,
        Location_Phone:null,
        Address:null,
        Locality:null,
        Consultation_Fee:null
      };

              function makeDaysString(binaryArray){
                var daysArray=["MON","TUE","WED","THU","FRI","SAT","SUN"];
                var dayStringArray=[];

                for (var i = 0; i <binaryArray.length; i++) {
                  if(binaryArray[i]==1){

                    for (var j = i+1; j <=binaryArray.length; j++) {
                      if(binaryArray[j]==0 || binaryArray[j]==undefined){
                        if(daysArray[i]!=daysArray[j-1]){
                          // j--;
                           dayStringArray.push({days:daysArray[i]+"-"+daysArray[j-1]});
                        }
                        else{
                          dayStringArray.push({days:daysArray[i]});
                        }

                        i=j;
                        break;
                      }
                    };
                  }

                };

                var dayString="";
                for (var i = 0; i < dayStringArray.length; i++) {
                  if(dayString.length>0){
                    dayString=dayString.concat(",",dayStringArray[i].days);
                  }
                  else{
                    dayString=dayString.concat(dayStringArray[i].days);
                  }
                }
                return dayString;
               }

              function returnTimeString(timingArray){
                var timeString="";
                for (var i = 0; i < timingArray.length; i++) {
                  if(timeString.length>0){
                    timeString=timeString.concat(" | ",timingArray[i].fromTime+"-"+timingArray[i].toTime);
                  }
                  else{
                    timeString=timeString.concat(timingArray[i].fromTime+"-"+timingArray[i].toTime);
                  }
                }
                return timeString;
              }

              $scope.doctorDetails=passPatientDetails.getData();


                $scope.inputs = [{
                    fromTime: "From Time",
                    toTime: "To Time"
                }];

                $scope.addInput = function () {
                    $scope.inputs.push({
                        fromTime: "From Time",
                        toTime: "To Time"
                    });
                }

                $scope.removeInput = function (index) {
                    $scope.inputs.splice(index, 1);
                }

                $scope.timeSlotChange=function(timeSlot){
                    $scope.timeSlotSelected=timeSlot.time;
                  }

              // Date Time Picker
            $scope.slots = {epochTime: 12600, format: 12, step: 15};
            function prependZero(param) {
            if (String(param).length < 2) {
               return "0" + String(param);
                }
                  return param;
            }

             function epochParser(val, opType) {
              if (val === null) {
                return "00:00";
              } else {
                var meridian = ['AM', 'PM'];

                  if (opType === 'time') {
                    var hours = parseInt(val / 3600);
                    var minutes = (val / 60) % 60;
                    var hoursRes = hours > 12 ? (hours - 12) : hours;

                    var currentMeridian = meridian[parseInt(hours / 12)];

                    return (prependZero(hoursRes) + ":" + prependZero(minutes) + " " + currentMeridian);
                  }
                }
            }

            $scope.fromTime=epochParser($scope.slots.epochTime,'time');

            $scope.fromTimePickerCallback = function (val) {
              if (typeof (val) === 'undefined') {
              } else {
                $scope.fromTime=epochParser(val,'time');
                $scope.toTime=epochParser(val+900,'time');

                    $scope.inputs[$scope.index].fromTime=$scope.fromTime;


              }
            };

            $scope.toTime=epochParser($scope.slots.epochTime,'time');
            $scope.toTimePickerCallback = function (val) {
              if (typeof (val) === 'undefined') {
              } else {
                $scope.toTime=epochParser(val,'time');

                    $scope.inputs[$scope.index].toTime=$scope.toTime;

              }
            };
            $scope.getIndex=function(index){
              $scope.index=index;
            }
                $scope.dayList = [
                {"day":"Monday",checked:false},
                {"day":"Tuesday",checked:false},
                {"day":"Wednesday",checked:false},
                {"day":"Thursday",checked:false},
                {"day":"Friday",checked:false},
                {"day":"Saturday",checked:false},
                {"day":"Sunday",checked:false},
                ]


                 $scope.timeSlotArray=[{
                  time:"15 Min",id:1},{
                  time:"30 Min",id:2},{
                  time:"45 Min",id:3},{
                  time:"1 Hour",id:4},{
                  time:"2 Hour",id:5}
                ];

             $scope.timeSlotSelected=$scope.timeSlotArray[0].time;

                $scope.newTimeSelected={
                  days:null,
                  timing:null,
                  time_slot:null,
                  binaryArray:null,
                  timeString:null
                };

            $scope.consultationFees=[];
              $scope.firstVisitFee={firstVisit:null}
              $scope.secondVisitFee={secondVisit:null};

                $scope.timeSlot=$scope.timeSlotArray[0];

                $scope.goToDoctorClinic = function(){

                function zeroPadding(n) {
                          if(n<10)
                          return ("000" + n);
                          else if (n>9 && n<100)
                          return ("00" + n);
                          else if (n>99 && n<1000)
                          return("0" + n);
                          else return(n);
                      }

              $ionicLoading.show({
                content: 'Updating',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
              });
                    $ionicHistory.nextViewOptions({
                    disableAnimate: true,
                    disableBack: true
                  });
                    var jsonServicesArray=[];
                for (var i = $scope.servicesSelected.length - 1; i >= 0; i--) {
                  jsonServicesArray.push({
                    "name": $scope.servicesSelected[i].name
                  });
                };
                var jsonTimingArray=[];

                for (var i = $scope.dayTimingArray.length - 1; i >= 0; i--) {
                  jsonTimingArray.push({
                    "days": $scope.dayTimingArray[i].days,
                    "timing":$scope.dayTimingArray[i].timing,
                    "time_slot":$scope.dayTimingArray[i].time_slot,
                    "binaryArray":$scope.dayTimingArray[i].binaryArray
                  });
                };

                            var totalCount;
                             var promise = $kinvey.DataStore.count('KidoroLocation');
                             promise.then(function(response) {
                              $scope.totalCount = ++response;
                            $scope.count = "LOC"+zeroPadding($scope.totalCount);
            $scope.consultationFees.push($scope.firstVisitFee);
            $scope.consultationFees.push($scope.secondVisitFee);
             var entity ={
                Location_ID: $scope.count,
                Address: $scope.clinic.Address,
                Consultation_Fee: $scope.consultationFees,
                Default_Location: $scope.clinic.Default_Location,
                Doctor_ID: $scope.doctorDetails.Doctor_ID,
                Hospital_Name: $scope.clinic.Hospital_Name,
                Hospital_Phone: Number($scope.clinic.Hospital_Phone),
                Locality: $scope.clinic.Locality,
                Default_Location:"Y",
                Timing: jsonTimingArray,
                Services:jsonServicesArray
                   }

                   var acl    = new $kinvey.Acl(entity);
                  var promise = $kinvey.DataStore.save('KidoroLocation', entity);
            promise.then(function(model) {
                $ionicLoading.hide();
                 $state.go('menu.doctorClinicDetails');

            }, function(err) {
                $ionicLoading.hide();
                // alert(err);
            });

            }, function(err) {
                            $ionicLoading.hide();
                            // alert("Opps! Something went wrong");
                         });
                };

            $ionicModal.fromTemplateUrl('templates/consultationHoursModal.html', {
             scope: $scope,
             animation: 'slide-in-up'
            }).then(function(consultationHoursModal) {
             $scope.consultationHoursModal = consultationHoursModal;
            });

            $scope.addconsultationHours = function() {
            $scope.dayTimingArray = [];
            $scope.consultationHoursModal.show();
            }

            $scope.dayTimingArray = [];

            $scope.consultationHoursDone=function(){

            $scope.tempBinaryArray=[];
            for (var i = 0; i <$scope.dayList.length; i++) {
              if($scope.dayList[i].checked==true){
                $scope.tempBinaryArray.push(1);
              }
              else{
                $scope.tempBinaryArray.push(0);
              }
            };
            var dayString=makeDaysString($scope.tempBinaryArray);
            var timeString=returnTimeString($scope.inputs);
            $scope.newTimeSelected.days=dayString;
            $scope.newTimeSelected.timing=$scope.inputs;
            $scope.newTimeSelected.time_slot=$scope.timeSlotSelected;
            $scope.newTimeSelected.binaryArray=$scope.tempBinaryArray;
            $scope.newTimeSelected.timeString=timeString;
            $scope.dayTimingArray.push($scope.newTimeSelected);

            $scope.consultationHoursModal.hide();

            }

            $scope.closeconsultationHours = function() {
            $scope.consultationHoursModal.hide();
            }

            $scope.$on('$destroy', function() {
             $scope.consultationHoursModal.remove();
            });

            // consultation Hours Modal

            // service Modal

            $ionicModal.fromTemplateUrl('templates/servicesListModal.html', {
              scope: $scope,
                 animation: 'slide-in-up',
                 backdropClickToClose: false,
                 hardwareBackButtonClose: false,
                 focusFirstInput: true
            }).then(function(serviceModal) {
             $scope.serviceModal = serviceModal;
             $scope.serviceModal.searchText = "";
            });

            $scope.serviceList = [];

            var promise = Kinvey.DataStore.get('KidoroDocServices');
            promise.then(function(entity) {
                for (var i = 0; i < entity.length; i++) {
                  $scope.serviceList.push({'name': entity[i].Service_Name});
                }
            }, function(error) {
            });

            $scope.openServiceModal = function() {
              $scope.serviceModal.show();
            }

            $scope.servicesSelected=[];
            $scope.servicesDone=function(){
              for (var i = 0; i < $scope.serviceList.length; i++) {
               if($scope.serviceList[i].checked==true){

                $scope.servicesSelected.push($scope.serviceList[i]);
               }
            }
             $scope.serviceModal.hide();
            }

            $scope.closeServiceModal = function() {
              $scope.serviceModal.hide();
            }

            $scope.$on('$destroy', function() {
               $scope.serviceModal.remove();
             });


          }catch(err){
                $ionicLoading.hide();
                $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
                 
                }, function (error) {
                                });
            }

 })

.controller("editDrProfileCtrl", function($scope,$kinvey, $cordovaToast, $ionicLoading,$state, $ionicModal, $ionicHistory,passPatientDetails){

try{


        $scope.doctorDetails=passPatientDetails.getData();

          $scope.clearSearch = function() {
            $scope.search = '';
          };
          $scope.editDrProfile = function() {
            $state.go('menu.editDrProfile');
          }

              $ionicHistory.nextViewOptions({
              disableAnimate: true,
              disableBack: true
            });

            $scope.goToDoctorClinic = function() {
              var jsonArray=[];
            for (var i = $scope.doctorDetails.Memberships.length - 1; i >= 0; i--) {
              jsonArray.push({
                "type": $scope.doctorDetails.Memberships[i].type
              });
            };
               $ionicLoading.show({
            content: 'Updating',
            animation: 'fade-in',
            showBackdrop: true,
            maxWidth: 200,
            showDelay: 0
          });
               var entity ={
            _id: $scope.doctorDetails._id,
            Days: $scope.doctorDetails.Days,
            Doc_Name: $scope.doctorDetails.Doc_Name,
            Doctor_ID: $scope.doctorDetails.Doctor_ID,
            Experience: Number($scope.doctorDetails.Experience),
            Kidoro_Doc_Code: $scope.doctorDetails.Kidoro_Doc_Code,
            Location_ID: $scope.doctorDetails.Location_ID,
            Memberships: jsonArray,
            Qualification: $scope.doctorDetails.qualificationArray,
            Reception_ID: $scope.doctorDetails.Reception_ID,
            Recommendations: $scope.doctorDetails.Recommendations,
            Reg_ID: $scope.doctorDetails.Reg_ID,
            Specaiality:$scope.doctorDetails.Specaiality,
            Specialization: $scope.doctorDetails.Specialization,
            Doc_Phone: $scope.doctorDetails.Doc_Phone,
            Doc_Locality: $scope.doctorDetails.Doc_Locality,
            Doc_Address: $scope.doctorDetails.Doc_Address,
            Doc_Pic: $scope.doctorDetails.Doc_Pic,
            Account_Status:$scope.doctorDetails.Account_Status
               }
               var acl    = new $kinvey.Acl(entity);
              var promise = $kinvey.DataStore.save('KidoroDocs', entity);
        promise.then(function(model) {
            $ionicLoading.hide();
             $state.go('menu.doctorClinicDetails');
        }, function(err) {
            // alert(err);
        });

            }



          $scope.newQualification={"Degree": null
          }

          $ionicModal.fromTemplateUrl('templates/qualificationModal.html', {
           scope: $scope,
           animation: 'slide-in-up'
         }).then(function(qualificationModal) {
           $scope.qualificationModal = qualificationModal;
         });

         $scope.openQualificationModal = function() {
           $scope.newQualification={"Degree": null
             }
            $scope.qualificationModal.show();
          };

          $scope.closeQualificationModal = function() {
            $scope.qualificationModal.hide();
          };

          $scope.updateQualification = function()
         {
          if (($scope.newQualification.Degree == "") || ($scope.newQualification.Degree == null) ) {
          } else {
            if( $scope.doctorDetails.qualificationArray!=null){
            $scope.doctorDetails.qualificationArray.push($scope.newQualification);
            }
            else{
              $scope.doctorDetails.qualificationArray=[];
               $scope.doctorDetails.qualificationArray.push($scope.newQualification);
            }

          }


           $scope.qualificationModal.hide();
         }

         $scope.$on('$destroy', function() {
            $scope.qualificationModal.remove();
          });

        // qualification Modal

        // experience Modal

         $ionicModal.fromTemplateUrl('templates/experienceModal.html', {
          scope: $scope,
          animation: 'slide-in-up'
        }).then(function(experienceModal) {
          $scope.experienceModal = experienceModal;
        });

        $scope.openExperienceModal = function() {
          $scope.experienceModal.show();
        }

        $scope.updateExperience=function(){
          $scope.experienceModal.hide();
        }

        $scope.closeExperienceModal = function() {
          $scope.experienceModal.hide();
        }

        $scope.$on('$destroy', function() {
           $scope.experienceModal.remove();
         });

        // experience Modal

        // membership Modal


        $scope.membershipList = [];
        var promise = Kinvey.DataStore.get('KidoroDocMemberships');
        promise.then(function(entity) {
            for (var i = 0; i < entity.length; i++) {
              $scope.membershipList.push({'type': entity[i].Membership_Name, 'checked': false});
            }
        }, function(error) {
        });

        $scope.newMembership={type: null, checked: false};



        $ionicModal.fromTemplateUrl('templates/membershipModal.html', {
          scope: $scope,
             animation: 'slide-in-up',
             backdropClickToClose: false,
             hardwareBackButtonClose: false,
             focusFirstInput: true
        }).then(function(membershipModal) {
         $scope.membershipModal = membershipModal;
         $scope.membershipModal.searchText = "";
        });

          $scope.clearSearch = function() {
            $scope.membershipModal.searchText = "";
          }

        $scope.openMembershipModal = function() {

          $scope.newMembership={type: null, checked: false};
          $scope.newMembership = $scope.doctorDetails.Memberships;


          $scope.membershipModal.show();
        }

          $scope.updateMembership=function(){
              $scope.doctorDetails.Memberships=[];
            for (var i = $scope.membershipList.length - 1; i >= 0; i--) {
              if($scope.membershipList[i].checked==true){
                $scope.newMembership={type: $scope.membershipList[i].type, checked:$scope.membershipList[i].checked };
                $scope.doctorDetails.Memberships.push($scope.newMembership);
              }
            };
            $scope.membershipModal.hide();
          }

          $scope.closeMembershipModal = function() {
            $scope.membershipModal.hide();
          }

          $scope.$on('$destroy', function() {
             $scope.membershipModal.remove();
           });

        // membership Modal

     }catch(err){
                $ionicLoading.hide();
                $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
                }, function (error) {
                });
            }
})


.controller('AnalysisReportCtrl', function($scope,$kinvey,$ionicLoading) {


  try{


    $ionicLoading.show({});

  function contains(a, obj) {
    var i = a.length;
    while (i--) {
       if (a[i] === obj) {
           return true;
       }
    }
    return false;
}
$scope.getTotalRevenue = function(){
   var query = new Kinvey.Query();
  query.equalTo('Doctor_ID', $scope.doctorId);
    var promise = $kinvey.DataStore.find('KidoroMedicalRecords',query);
  promise.then(function(models) {
    var currentMonth = new Date().getMonth()+1;
    $scope.graph.series.push(monthNames[currentMonth-1]);

    for (var i = 0; i < models.length; i++) {
      if(typeof (models[i].Record_Date) !== 'undefined'){
      var recordMonth = Number(models[i].Record_Date.split("-")[1]);
      if(recordMonth==currentMonth){
       
        $scope.totalAmount+=Number(models[i].Total_Fee);
    
      }

      }

    };


  },function(err){
    $ionicLoading.hide();
           // alert('Failed '+err.description);
    });
}

$scope.getTotalAppointments = function(){
   var query = new Kinvey.Query();
  query.equalTo('Doctor_ID', $scope.doctorId);
    var promise = $kinvey.DataStore.find('KidoroAppointment',query);
  promise.then(function(models) {
    var currentMonth = new Date().getMonth()+1;
    $scope.graph.series.push(monthNames[currentMonth-1]);
    $scope.graph.plot={"x":[],"y":[]};
    $scope.x = [];
    $scope.y = [];
    for (var i = 0; i < models.length; i++) {
      if(typeof (models[i].Appointment_Date) !== 'undefined'){
      var recordMonth = Number(models[i].Appointment_Date.split("-")[1]);
      if(recordMonth==currentMonth){
        $scope.x.push(Number(models[i].Appointment_Date.split("-")[0]));
        $scope.y.push(1);
     

      }

      }

    };
    var dateToCompare=null;
    for (var i = 0; i <$scope.x.length; i++) {

      if(i==0){
      dateToCompare=Number($scope.x[0]);
      }
      if(dateToCompare==Number($scope.x[i])){
        $scope.noOfAppointments++;
      }
      if((dateToCompare!=null&&dateToCompare!=Number($scope.x[i]))){
      $scope.graph.plot.x.push(dateToCompare);
      $scope.graph.plot.y.push( $scope.noOfAppointments);
      dateToCompare=Number($scope.x[i]);
      $scope.noOfAppointments=1;
      }
      if(i==($scope.x.length-1)){
      $scope.graph.plot.x.push(dateToCompare);
      $scope.graph.plot.y.push( $scope.noOfAppointments);
      }
    }
    $scope.totalNoofAppointment = $scope.y.length;
     $scope.graph.data.push($scope.graph.plot.y);
     $scope.graph.labels=$scope.graph.plot.x;
   
    $ionicLoading.hide();

  },function(err){
    $ionicLoading.hide();
           alert('Failed '+err.description);
    });
}

var monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"];
  $scope.graph = {};
  $scope.graph.color = ['#009999'];
  $scope.graph.data = [];
  $scope.graph.labels = [];
  $scope.graph.series=[];
  $scope.monthRevenue=[];
  $scope.monthDate=[];
  $scope.totalAmount=0;
  $scope.noOfAppointments=0;
  $scope.getTotalAppointments();
  $scope.getTotalRevenue();
 


   }catch(err){
                $ionicLoading.hide();
                $cordovaToast.show("Can't connect right now", 1500, "bottom").then(function(success) {
                }, function (error) {
                });
            }
})

angular.module('ion-fab-button', [])
    .directive('fabButton', function fabButtonDirective() {
        return {
            restrict: 'E',
            replace: true,
            transclude: true,
            template: template,
            link: link
        };
        
        function isAnchor(attr) {
            return angular.isDefined(attr.href) || angular.isDefined(attr.ngHref);
        }
        function template(element, attr) {
            return isAnchor(attr) ?
                '<a class="fab-button" ng-transclude></a>' :
                '<button class="fab-button" ng-transclude></button>';
        }
        function link(scope, element, attr) {
            var target = '#'+attr['targetId'];
            var targetEl = angular.element(document.querySelector(target));
            var savePos = 0;
            targetEl.bind('scroll', function (e) {
                if (savePos < e.detail.scrollTop) {
                    savePos = e.detail.scrollTop;
                    element.removeClass('fadeInUp animated');
                    element.addClass('fadeOutDown animated');
                }
                if (savePos > e.detail.scrollTop) {
                    savePos = e.detail.scrollTop;
                    element.removeClass('fadeOutDown animated');
                    element.addClass('fadeInUp animated');
                }
            });
        }
    });

