angular.module('starter.services', [])

  .factory('privateMessage', ['$firebase', function($firebase) {

      var ref = new Firebase('https://kidorochatclient.firebaseio.com');
      var messages = $firebase(ref.child('messages')).$asArray();
      var privateMessage = {
        all: function(id) {
          var messages = $firebase(ref.child(id)).$asArray();
          return messages;
        },
        create: function (message, id) {

          var id = $firebase(ref.child(id)).$asArray();
          return id.$add(message);
        },
        get: function (messageId) {
           
          return $firebase(ref.child('messages').child(messageId)).$asObject();
        },
        delete: function (message) {
          return messages.$remove(message);
        }
      };

      return privateMessage;

    }])


.factory('passPatientDetails',function($stateParams){
    var savedData = {};
        return {
            getData : function(){
            return savedData;
            },
            setData: function(item){
              savedData = item;

            }
        }
    })


    .factory('passDoctorDetails',function($stateParams){
        var savedData = {};
            return {
                getData : function(){
                return savedData;
                },
                setData: function(item){
                  savedData = item;

                }
            }
        })

        .factory('passPrescriptionData',function($stateParams){
            var savedData = {};
                return {
                    getData : function(){
                    return savedData;
                    },
                    setData: function(item){
                      savedData = item;

                    }
                }
            })

        .factory('userDataService',function($stateParams){
        var savedData = {};
            return {
                getData : function(){
                return savedData;
                },
                setData: function(data){
                  savedData = data;

                }
            }
        })


.factory('passLocationDetails',function($stateParams){
    var savedData = {};
        return {
            getData : function(){
            return savedData;
            },
            setData: function(item){
              savedData = item;

            }
        }
    })
.factory('sendSMSServices',['$http',function($http, $scope, $stateParams, $ionicPopup){

      try {

        return {
            getRespose : function(number,message) {
              var authKey="91802AFF83DNnxK55fc18a1";

            $http.get("https://control.msg91.com/api/sendhttp.php?authkey="+authKey+"&mobiles="+number+"&message="+message+"&sender=Kidoro&route=4&country=91")
             .success(function(result) {
             
             })
             .error(function(data) {
             
             });
           }
        }

      } catch (err) {
  var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
        text: savedlang.okTxt,
        type: 'button-alert-ok',
        onTap: function(e) {
          errorAlert.close();

    }
  }]
});
      }

  }])
.factory('patientListService',function($stateParams){
    var savedData = {};
        return {
            getData : function(){
            return savedData;
            },
            setData: function(item){
              savedData = item;

            }
        }
    })
  .factory('getPatientDetails',function($stateParams,$kinvey,patientListService){
    function contains(a, obj) {
    var i = a.length;
    while (i--) {
       if (a[i] === obj) {
           return true;
       }
    }
    return false;
}
        var patientsList = [];
            return {
                getData : function(doctorId){

      
      var query = new Kinvey.Query();
      query.equalTo('Doctor_ID', doctorId);
      var promise = $kinvey.DataStore.find('KidoroAppointment',query);
      promise.then(function(model) {
       
     User_IDArray=[];
     for (var i = 0; i<model.length; i++) {
      if(!contains(User_IDArray,model[i].User_ID)){
        User_IDArray.push(model[i].User_ID);
      }
    };
   
    for (var j = 0; j<User_IDArray.length; j++) {
      var query = new Kinvey.Query();
       query.equalTo('Kidoro_Code',User_IDArray[j]);
      var promise = $kinvey.DataStore.find('KidoroUser',query);
      promise.then(function(model) {
   
        if(model.length>0){
   
        if((model[0]["Profile_Pic_URI"]===undefined) || (model[0]["Profile_Pic_URI"]==null)){
          model[0]["Profile_Pic_URI"] = "./img/noimage.png";
        }

        if(model[0]["Child_Date"]!=null){
        var year = Number(model[0]["Child_Date"].split("-")[2]);
        var today=new Date();
        model[0]["Age"]=today.getFullYear()-year;
        }
        if(model[0].User_Type=="Offline"){
          model[0]["showInvite"]=true;
          model[0]["showChat"]=false;
        }
        else{
           model[0]["showInvite"]=false;
          model[0]["showChat"]=true;
        }
        patientsList.push(model[0]);
        patientListService.setData(patientsList);
      }
      },function(err){
        $ionicLoading.hide();

           alert('Failed '+err.description);
    });
    };

 return patientsList;
  },function(err){
    $ionicLoading.hide();

           alert('Failed '+err.description);
    });
                }
            }
        })


  .factory('sendMail',['$http',function($http, $scope, $stateParams, $ionicPopup){

      try {
        return {
            getRespose : function(subject, template_name, email, name, status, id, date, time, cname, doctorName){

              $http.post('https://mandrillapp.com/api/1.0/messages/send-template.json',
              {
    "key": "3LgvpWOf9LxZCUHf36IF8w",
    "template_name": template_name,
    "template_content": [
        {
            "name": "userName",
            "content": name,
        }
    ],
    "message": {
      "global_merge_vars": [
                {
                    "name": "name",
                    "content": name
                },
                {
                    "name": "status",
                    "content": status
                },
                {
                    "name": "id",
                    "content": id
                },
                {
                    "name": "date",
                    "content": date
                },
                {
                    "name": "time",
                    "content": time
                },
                {
                    "name": "cname",
                    "content": cname
                },
                {
                    "name": "doctorName",
                    "content": doctorName
                }
            ],
        "subject": subject,
        "from_email": "info@tarantulalabs.com",
        "from_name": "Kidoro Health",
        "to": [
            {
                "email": email,
                "name": name,
                "type": "to"
            }
        ],
        "headers": {
            "Reply-To": "info@tarantulalabs.com"
        }


    }

}

            ).
      then(function(response) {
      }, function(response) {

      });
      }
        }
      } catch (err) {
  var errorAlert = $ionicPopup.alert({
       title: 'Kidoro Health',
       template: savedlang.sometngwentwrng,
       cssClass:'custom-popup-alert',
        buttons: [{
        text: savedlang.okTxt,
        type: 'button-alert-ok',
        onTap: function(e) {
          errorAlert.close();

    }
  }]
});
      }

  }])


.factory('kinveyInitService',function($stateParams,$kinvey){
        return {
            getResponse : function(){
            var promise =  $kinvey.init({
            appKey    : 'kid_WyB3Qm2kze',
            appSecret : 'dff253b755e8466ab4d3be7a059b2ee5',
           sync      : { enable: true } //------OfflineSync
        });

    promise.then(function(initial) {

     
            var user = $kinvey.getActiveUser();

          if(null == user) {
            var promise = $kinvey.User.login('maaniksinha', 'nokia@7610');
             promise.then(function(user) {
            // alert(' login Success '+user);
            return user;

             },
             function(error) {
              // alert('Login Failed. Response: ' + error.description);
            });
             }

            },
            function(err) {

           // alert('Initilize Failed '+err.description);
          });
        }
      }
    });
