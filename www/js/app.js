
angular.module('starter', ['ionic','flexcalendar','ion-fab-button', 'ui.bootstrap.datetimepicker','firebase', 'ionic-timepicker','ngAnimate','starter.controllers','starter.services','starter.directives','kinvey','angular.filter','chart.js','ionic-timepicker','ngCordova'])

.run(function($ionicPlatform,$kinvey, $state,$ionicHistory, $rootScope,$timeout,userDataService,getPatientDetails,$ionicScrollDelegate,$cordovaLocalNotification,$ionicHistory) {
  $ionicPlatform.ready(function() {
  
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }

    if (window.StatusBar) {
     
      StatusBar.styleLightContent();
    }
    if(window.Connection) {
      if(navigator.connection.type == Connection.NONE) {
        $ionicPopup.confirm({
          title: 'No Internet Connection',
          content: 'Sorry, no Internet connectivity detected. Please reconnect and try again.'
        })
        .then(function(result) {
          if(!result) {
            ionic.Platform.exitApp();
          }
        });
      }
    }

$ionicPlatform.registerBackButtonAction(function (event) {
  if($state.current.name=="menu.appointments"){
    navigator.app.exitApp(); //<-- remove this line to disable the exit
  }
  else if($state.current.name=="menu.patients"||$state.current.name=="menu.AnalysisReport"||$state.current.name=="menu.doctorClinicDetails"){
  $ionicHistory.clearHistory();
  $state.go('menu.appointments');
  }
 
}, 100);



if(window.plugin && window.plugin.notification) { 
  window.plugin.notification.local.onadd = function (id, state, json) {
            var notification = {
                id: id,
                state: state,
                json: json
            };
            $timeout(function() {
                $rootScope.$broadcast("$cordovaLocalNotification:added", notification);
            });
        };
}
            var promise = $kinvey.init({
                appKey : 'kid_WyB3Qm2kze',
                appSecret : 'dff253b755e8466ab4d3be7a059b2ee5',
                 sync      : { enable: true }
            });
            promise.then(function() {

  var ref = new Firebase('https://kidorochatclient.firebaseio.com');

                  ref.on('value', function(snapshot) {


                 

         if($ionicHistory.currentStateName()=='menu.chat'){
          $ionicScrollDelegate.scrollBottom(true);

         }
         else{
            if($ionicHistory.currentStateName().length==0){
            }
            else{
            $cordovaLocalNotification.add({
            id: "1234",
            date: new Date(),
            message: "You have a new message",
            title: "Kidoro Health",
            autoCancel: true,
            sound: true
             }).then(function () {
         
            });
            }
            
      }
});
                determineBehavior($kinvey, $state, $rootScope,$timeout,userDataService,getPatientDetails);


            }, function(errorCallback) {

                determineBehavior($kinvey, $state, $rootScope,$timeout,userDataService,getPatientDetails);
            });
  });
})

.config(function($stateProvider, $urlRouterProvider,$ionicConfigProvider) {

  $ionicConfigProvider.backButton.text('').icon('ion-ios-arrow-left').previousTitleText(false);
  $ionicConfigProvider.form.checkbox('circle');


$stateProvider

.state('home', {
    url: '/home',
    templateUrl: 'templates/home.html'

})

.state('tempSplash', {
        url: '/tempSplash',
        templateUrl: 'templates/tempSplash.html',
        controller: 'tempSplashCtrl'
     })

.state('loginHome', {
        url: '/loginHome',
        templateUrl: 'templates/loginHome.html',
        controller: 'loginHomeCtrl'
    })

.state('welcomeLogin', {
        url: '/welcomeLogin',
        templateUrl: 'templates/welcomeLogin.html',
        controller: 'welcomeLoginCtrl'
      })

.state('registerHome', {
          url: '/registerHome',
          templateUrl: 'templates/registerHome.html',
          controller: 'registerHomeCtrl'
      })

.state('registerHomeNext', {
        url: '/registerHomeNext',
        templateUrl: 'templates/registerHomeNext.html',
        controller: 'registerHomeNextCtrl'
        })

.state('registerSuccess', {
        url: '/registerSuccess',
        templateUrl: 'templates/registerSuccess.html',
        controller: 'registerSuccessCtrl'
        })

.state('forgotPassword', {
        url: '/forgotPassword',
        templateUrl: 'templates/forgotPassword.html',
        controller: 'forgotPasswordCtrl'
     })


    .state('menu', {
    url: '/menu',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'MenuCtrl'
  })

  // code by Maanik

  .state('menu.saveMedicalRecord', {
      url: '/saveMedicalRecord',
      views:{
        'menuContent':{
          templateUrl: 'templates/saveMedicalRecord.html',
          controller: 'saveMedicalRecordCtrl'
        }
      }

    })

    .state('menu.savedMedicalRecord', {
        url: '/savedMedicalRecord',
        views:{
          'menuContent':{
            templateUrl: 'templates/savedMedicalRecord.html',
            controller: 'savedMedicalRecordCtrl'
          }
        }

      })

    // code by Maanik

.state('menu.appointments', {
  cache:false,
    url: '/appointments',
    views: {
      'menuContent': {
        templateUrl: 'templates/appointments.html',
        controller: 'AppointmentCtrl'
      }
    }
  })

  .state('menu.patients', {
    cache:true,
    url: '/patients',
    views: {
      'menuContent': {
        templateUrl: 'templates/patients.html',
        controller: 'patientCtrl'
      }
    }
  })
  .state('menu.AnalysisReport', {
     cache:false,
    url: '/AnalysisReport',
    views: {
      'menuContent': {
        templateUrl: 'templates/analysisReports.html',
        controller: 'AnalysisReportCtrl'
      }
    }
  })

  .state('menu.patientDetails', {
     cache:false,
    url: '/patientDetails',
    views: {
      'menuContent': {
        templateUrl: 'templates/patientDetails.html',
        controller: 'patientDetailsCtrl'
      }
    }
  })

  .state('menu.doctorClinicDetails', {
    url: '/doctorClinicDetails',
     cache:true,

    views: {
      'menuContent': {
        templateUrl: 'templates/doctorClinicDetails.html',
        controller: 'doctorClinicDetailsCtrl'
      }
    }
  })

  .state('menu.editClinic', {
     cache:true,
    url: '/editClinic',
    views: {
      'menuContent': {
        templateUrl: 'templates/editClinic.html',
        controller: 'editClinicCtrl'
      }
    }
  })

  .state('menu.addClinic', {
     cache:true,
    url: '/addClinic',
    views: {
      'menuContent': {
        templateUrl: 'templates/addClinic.html',
        controller: 'addClinicCtrl'
      }
    }
  })

  .state('menu.editDrProfile', {
     cache:true,
    url: '/editDrProfile',
    views: {
      'menuContent': {
        templateUrl: 'templates/editDrProfile.html',
        controller: 'editDrProfileCtrl'
      }
    }
  })

    .state('menu.chat', {
     cache:true,
    url: '/chat',
    views: {
      'menuContent': {
        templateUrl: 'templates/chat.html',
        controller: 'chatCtrl'
      }
    }
  })



  $urlRouterProvider.otherwise('/tempSplash');
})
function determineBehavior($kinvey, $state, $rootScope,$timeout,userDataService,getPatientDetails) {

     $state.go('tempSplash');
    function navigateToHome() {
     $state.go('menu.appointments');
}
 function navigateToLogin() {
     $state.go('loginHome');
}
    var activeUser = $kinvey.getActiveUser();
   
    if ((activeUser === null)) {

             $timeout(navigateToLogin, 3000);

    } else if (($state.current.name !== 'homePage') && (activeUser !== null)) {

       if(activeUser.AccountType=="Doctor"){
        var query = new $kinvey.Query();
          query.equalTo('Kidoro_Doc_Code', activeUser.kidoroCode);
          var promise = $kinvey.DataStore.find('KidoroDocs', query);
          promise.then(function(models) {
          
        
          getPatientDetails.getData(models[0].Doctor_ID);
          userDataService.setData(models[0]);
            $timeout(function(){
               $state.go('menu.appointments');
            }, 15000);

          }, function(err) {

        });

    }
    else{
       var promise = Kinvey.User.logout();
    promise.then(function() {
      
      
         $timeout(navigateToLogin, 3000);
    }, function(error) {

    });
    }         
    }
};
